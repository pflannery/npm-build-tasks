import { dirname, resolve, sep } from 'path';
import { fileURLToPath } from 'url';

export class ModuleFileLocation {

  constructor(readonly importMeta: any, ...paths: string[]) {
    const compiledTestPath = importMeta.dirname
      // backward compatibility
      ?? dirname(fileURLToPath(importMeta.url));

    this.testSourcePath = compiledTestPath.replace(`${sep}out${sep}test`, `${sep}test`);

    this.paths.push(...paths);
  }

  private paths: string[] = [];

  private testSourcePath: string;

  join(...paths: string[]) {
    return new ModuleFileLocation(this.importMeta, ...this.paths, ...paths);
  }

  toString() {
    return resolve(this.testSourcePath, ...this.paths)
  }

}