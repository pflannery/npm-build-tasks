import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { readFile, rm, unlink } from 'node:fs/promises';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import { sanitizeOutput } from '../testSpawn.js';
import LogFixtures from './log.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const LogTests = {

  "writes to log file using cli switch -lf (no ansi espace codes)": async function () {
    const testLogFilePath = './log.file.cli.switch.log';
    const testLogFile = cwd.join(testLogFilePath).toString();
    const expected = LogFixtures.log_output!.substring(1);

    try {
      await new CliSpawnBuilder()
        .styleDisable()
        .logFile(testLogFilePath)
        .task('test')
        .spawn(cwd.toString());

      const actual = await readFile(testLogFile, 'utf8');
      equal(sanitizeOutput(actual), expected);
    } catch (reason: any) {
      fail(reason);
    }

    try { await unlink(testLogFile); } catch { }
  },

  "writes to log.filePath from tasks.yml (no ansi espace codes)": async function () {
    const testLogFile = cwd.join('./log.file.tasks.log').toString();
    const expected = LogFixtures.log_output!.substring(1);
    try {
      await new CliSpawnBuilder()
        .styleDisable()
        .task('test')
        .spawn(cwd.toString());

      const actual = await readFile(testLogFile, 'utf8');
      equal(sanitizeOutput(actual), expected);
    } catch (reason: any) {
      fail(reason);
    }

    try { await unlink(testLogFile); } catch { }
  },

  "writes to log file using cli logLevel (-lfl)": async function () {
    const logLevelPath = cwd.join('./logLevel');
    const testTasksFilePath = logLevelPath.join('tasks.yml').toString();
    const testLogFilePath = logLevelPath.join('log.file.level.cli.log').toString();
    const expected = LogFixtures.log_cli_level_output!.substring(1);
    try {
      await new CliSpawnBuilder()
        .styleDisable()
        .taskFile(testTasksFilePath)
        .logFile(testLogFilePath)
        .logFileLevel(ShellLogLevelNames.command)
        .task('test')
        .spawn(cwd.toString());

      const actual = await readFile(testLogFilePath, 'utf8');
      equal(sanitizeOutput(actual), expected);
    } catch (reason: any) {
      fail(reason);
    }

    try { await unlink(testLogFilePath); } catch { }
  },

  "creates log file directory when not exists": async function () {
    const logMkDirPath = cwd.join('./mkdir');
    const autoCreateDirPath = logMkDirPath.join('auto-create');
    const testLogFilePath = autoCreateDirPath.join('log.file.mkdir.cli.log').toString();
    const expected = LogFixtures.auto_create_missing_dir!.substring(1);

    try {
      await new CliSpawnBuilder()
        .logFile(testLogFilePath)
        .logFileLevel(ShellLogLevelNames.command)
        .task('test')
        .spawn(logMkDirPath.toString());

      const actual = await readFile(testLogFilePath, 'utf8');
      equal(sanitizeOutput(actual), expected);
    } catch (reason: any) {
      fail(reason);
    }

    try { await rm(autoCreateDirPath.toString(), { recursive: true }); } catch { }
  }

}