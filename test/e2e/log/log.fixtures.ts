import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  log_output: `
Executing [test]

1: [test] node -e "process.stdout.write('\\033[33mNo ansi escape codes\\033[39m\\n')"
No ansi escape codes

[test] completed 1 task(s)
`,

  log_cli_level_output: `
test log level
`,

  auto_create_missing_dir: `
test log file mkdir
`
}