import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  group_repeat: `
Executing [group_repeat]

1: [group_repeat][0] echo $CODE_NAME-$OS
current-alpine

2: [group_repeat][1] echo $CODE_NAME-$OS
current-bookworm

3: [group_repeat][2] echo $CODE_NAME-$OS
iron-alpine

4: [group_repeat][3] echo $CODE_NAME-$OS
iron-bookworm

5: [group_repeat][4] echo $CODE_NAME-$OS
hydrogen-alpine

6: [group_repeat][5] echo $CODE_NAME-$OS
hydrogen-bookworm

[group_repeat] completed 6 task(s)
`,

  group_child_task_repeat: `
Executing [group_child_task_repeat]

1.1: [child_task][0] echo $TEST_CHILD
1

2.1: [child_task][1] echo $TEST_CHILD
2

3.1: [child_task][2] echo $TEST_CHILD
3

[group_child_task_repeat] completed 3 task(s)
`,

  group_set_var_repeat: `
Executing [group_set_var_repeat]

1: [group_set_var_repeat][0] echo $REPEAT_SET_VAR
current-alpine

2: [group_set_var_repeat][1] echo $REPEAT_SET_VAR
current-bookworm

3: [group_set_var_repeat][2] echo $REPEAT_SET_VAR
iron-alpine

4: [group_set_var_repeat][3] echo $REPEAT_SET_VAR
iron-bookworm

5: [group_set_var_repeat][4] echo $REPEAT_SET_VAR
hydrogen-alpine

6: [group_set_var_repeat][5] echo $REPEAT_SET_VAR
hydrogen-bookworm

[group_set_var_repeat] completed 6 task(s)
`,

group_set_env_repeat: `
Executing [group_set_env_repeat]

1: [group_set_env_repeat][0] echo $REPEAT_SET_ENV
debug-x86

2: [group_set_env_repeat][1] echo $REPEAT_SET_ENV
debug-x64

3: [group_set_env_repeat][2] echo $REPEAT_SET_ENV
release-x86

4: [group_set_env_repeat][3] echo $REPEAT_SET_ENV
release-x64

[group_set_env_repeat] completed 4 task(s)
`

}