import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import RepeatFixtures from './repeat.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const RepeatTests = {

  "executes '$1'": [
    'group_repeat',
    'group_child_task_repeat',
    'group_set_var_repeat',
    'group_set_env_repeat',
    async function (testTaskName: string) {
      const expected = RepeatFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}