import type { KeyMap } from '#utils';
import { dirname, resolve } from 'path';
import { fileURLToPath } from 'url';
import { spawnTest } from './testSpawn.js';

export const moduleFilePath = import.meta.dirname
  // backward compatibility
  ?? dirname(fileURLToPath(import.meta.url));

export const compiledOutPath = resolve(moduleFilePath, '..', '..');

export class CliSpawnBuilder {

  constructor() {
    this.args.push('node');
    this.args.push(resolve(compiledOutPath, './src/index.js'));
    this.disableTaskTime();
  }

  private args: string[] = [];

  task(...taskName: string[]) {
    this.args.push(...taskName);
    return this;
  }

  taskArgs(args: string) {
    this.args.push(` -- ${args}`);
    return this;
  }

  taskFile(taskFilePath: string) {
    this.args.push(`-tf ${taskFilePath}`);
    return this;
  }

  logLevel(logLevel: string) {
    this.args.push(`-l ${logLevel}`);
    return this;
  }

  logFile(logFilePath: string) {
    this.args.push(`-lf ${logFilePath}`);
    return this;
  }

  logFileLevel(logLevel: string) {
    this.args.push(`-lfl ${logLevel}`);
    return this;
  }

  envFile(envFilePath: string) {
    this.args.push(`-ef ${envFilePath}`);
    return this;
  }

  env(env: KeyMap<string | number | boolean>) {
    for (const name in env) {
      this.args.push(`-e ${name}=${env[name]}`);
    }

    return this;
  }

  var(vars: KeyMap<string>) {
    for (const name in vars) {
      this.args.push(`-v ${name}=${vars[name]}`);
    }

    return this;
  }

  shellMode(mode: string) {
    this.args.push(`-sm ${mode}`);
    return this;
  }

  styleDisable() {
    this.args.push('-sd');
    return this;
  }

  styleStep(...styles: string[]) {
    for (const style of styles) {
      this.args.push(`-ss ${style}`);
    }
    return this;
  }

  styleError(...styles: string[]) {
    for (const style of styles) {
      this.args.push(`-se ${style}`);
    }
    return this;
  }

  nestedNumbering() {
    this.args.push('-ons');
    return this;
  }

  disableTaskTime() {
    this.args.push('-odtt');
    return this;
  }

  listTasks() {
    this.args.push('-ls');
    return this;
  }

  version() {
    this.args.push('--version');
    return this;
  }

  help() {
    this.args.push('--help');
    return this;
  }

  raw(raw: string) {
    this.args.push(raw);
    return this;
  }

  toString() {
    return this.args.join(' ')
  }

  spawn(cwd: string): Promise<string> {
    return spawnTest(`${this}`, cwd)
  }

}