import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  schema_errors: `
Failed to parse ./invalid/schema.tasks.yml
error: '$' => cannot have a property called 'noAdditional'
error: '$.shell' => cannot have a property called 'noAdditional'
error: '$.shell.mode' => must be equal to 'tty|plain'
error: '$.shell.logLevel' => must be equal to 'step|command|error|silent'
error: '$.output' => cannot have a property called 'noAdditional'
error: '$.output.nestedSteps' => must be boolean
error: '$.style' => cannot have a property called 'noAdditional'
error: '$.style.disable' => must be boolean
error: '$.style.step' => must be array
error: '$.style.error' => must be array
error: '$.env' => '$invalid_env_name' is an invalid property name
error: '$.env.invalid_env_value' => must be string|boolean|number
error: '$.var' => '$invalid_var_name' is an invalid property name
error: '$.var.invalid_var_value' => must be string|boolean|number
error: '$.mask' => must be array
`,

  schema_null_errors: `
Failed to parse ./invalid/null.tasks.yml
error: '$.shell' => must be object
error: '$.output' => must be object
error: '$.style' => must be object
error: '$.env' => must be object
error: '$.var' => must be object
error: '$.mask' => must be array
error: '$.tasks' => must be object
`,

}