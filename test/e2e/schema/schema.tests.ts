import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import SchemaInvalidFixtures from './invalid/schema.fixtures.js';
import SchemaTypesFixtures from './types/schema.types.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const SchemaTests = {

  "handles schema errors in $1": [
    ['./invalid/schema.tasks.yml', 'schema_errors'],
    ['./invalid/null.tasks.yml', 'schema_null_errors'],
    async function (testTasksFilePath: string, testFixtureKey: string) {
      const expected = SchemaInvalidFixtures[testFixtureKey]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .taskFile(testTasksFilePath)
          .task('dummy')
          .spawn(cwd.toString());

        fail();
      } catch (actual: any) {
        equal(actual, expected);
      }
    }
  ],

  "'$1': schema tasks types supported": [
    'test_direct_global_types',
    'test_group_scope_types',
    async (testTaskName: string) => {
      const expected = SchemaTypesFixtures[testTaskName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .logLevel(ShellLogLevelNames.command)
          .taskFile('./types/tasks.yml')
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}