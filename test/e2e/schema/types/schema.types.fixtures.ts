import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  test_direct_global_types: `
true 1 global test string
`,

  test_group_scope_types: `
false 2 scope test string
`,

}