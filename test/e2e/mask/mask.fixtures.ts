import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  test_mask_env: `
$\{TEST_ENV_MASK\}
`,

  test_mask_var: `
$\{TEST_VAR_MASK\}
`,

  test_mask_group: `
$\{TEST_ENV_MASK\}
$\{TEST_VAR_MASK\}
`,

test_mask_error: `
masked1=$\{TEST_ENV_MASK\}
masked2=$\{TEST_VAR_MASK\}
Exited with error code 1
>>>: [test_mask_error] failed
`,

test_mask_error_group: `
masked1=$\{TEST_ENV_MASK\}
masked2=$\{TEST_VAR_MASK\}
Exited with error code 1
>>>: [test_mask_error] failed
`

}