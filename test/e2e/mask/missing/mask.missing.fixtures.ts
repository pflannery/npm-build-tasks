import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  mask_not_exist: `
Executing [mask_not_exist]

Could not find the mask variable 'TEST_MISSING' in env or var
`,

mask_not_exist_group: `
Executing [mask_not_exist_group]

Could not find the mask variable 'TEST_MISSING' in env or var
`

}