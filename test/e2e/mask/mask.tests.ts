import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import MaskFixtures from './mask.fixtures.js';
import MaskMissingFixtures from './missing/mask.missing.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const MaskTests = {

  "catches non-existing mask entries in $1": [
    ['mask_not_exist'],
    ['mask_not_exist_group'],
    async function (testTaskName: string) {
      const expected = MaskMissingFixtures[testTaskName]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .taskFile('./missing.mask.tasks.yml')
          .task(testTaskName)
          .spawn(cwd.join('missing').toString());

        fail();
      } catch (actual: any) {
        equal(actual, expected);
      }
    }
  ],

  "case $i: masks env and var stdout values": [
    ['test_mask_env'],
    ['test_mask_var'],
    ['test_mask_group'],
    async function (testTaskName: string) {
      const expected = MaskFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .logLevel(ShellLogLevelNames.command)
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "case $i: masks env and var error values": [
    ['test_mask_error'],
    ['test_mask_error_group'],
    async function (testTaskName: string) {
      const expected = MaskFixtures[testTaskName]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .styleDisable()
          .logLevel(ShellLogLevelNames.command)
          .task(testTaskName)
          .spawn(cwd.toString());

        fail();
      } catch (actual: any) {
        equal(actual, expected);

      }
    }
  ],

}