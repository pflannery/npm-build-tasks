import { spawn } from 'node:child_process';

const windows = process.platform === 'win32';

const buggerMessages = [
  'Debugger attached.\n',
  'Waiting for the debugger to disconnect...\n',
];

const stripBuggerNoise = (initial: string) => buggerMessages.reduce(
  (text, replace) => text.replace(replace, ''),
  initial
);

const replaceCrLf = (text: string) => windows
  ? text.replaceAll('\r\n', '\n')
  : text;

export const sanitizeOutput = (text: string) => {
  const noCrLf = replaceCrLf(text);
  return stripBuggerNoise(noCrLf);
}

export function spawnTest(command: string, cwd: string): Promise<string> {
  const options = {
    stdio: 'pipe',
    shell: true,
    cwd
  }

  return new Promise(
    (success, fail) => {
      const buffer: string[] = [];

      function onData(d: any) {
        buffer.push(sanitizeOutput(d.toString()));
      }

      const cp = spawn(command, [], <any>options);

      cp.stdout?.on('data', onData);
      cp.stderr?.on('data', onData);
      cp.once('error', fail);
      cp.once('close', (code: any) => {
        if (code === 0) return success(buffer.join(''));
        fail(buffer.join(''));
      });
    }
  );
}
