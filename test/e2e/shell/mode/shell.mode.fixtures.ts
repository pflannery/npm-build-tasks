import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  invalid_shell_mode_string_switch: `
Invalid shell mode 'invalidMode'. Valid modes are tty|plain
`,

  invalid_shell_mode_number_switch: `
Invalid shell mode '0'. Valid modes are tty|plain
`,

  invalid_shell_mode_yaml_string: `
Failed to parse ./invalid.mode.string.tasks.yml
error: '$.shell.mode' => must be equal to 'tty|plain'
`,

invalid_shell_mode_yaml_number: `
Failed to parse ./invalid.mode.number.tasks.yml
error: '$.shell.mode' => must be equal to 'tty|plain'
`,

  valid_shell_mode_string_switch: `
ok
`,

}