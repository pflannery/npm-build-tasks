import { ShellLogLevelNames, ShellMode } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import ShellModeFixtures from './shell.mode.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const ShellModeTests = {

  "handles invalid switch '$1'": [
    ['invalidMode', 'invalid_shell_mode_string_switch'],
    ['0', 'invalid_shell_mode_number_switch'],
    async function (testMode: string, fixtureKey: string) {
      const expected = ShellModeFixtures[fixtureKey]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .shellMode(testMode)
          .task('never-run-task')
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

  "handles valid switch '$1'": [
    [ShellMode.tty, 'valid_shell_mode_string_switch'],
    [ShellMode.plain, 'valid_shell_mode_string_switch'],
    async function (testMode: string, testFixtureName: string) {
      const expected = ShellModeFixtures[testFixtureName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .logLevel(ShellLogLevelNames.command)
          .shellMode(testMode)
          .task('ok_task')
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "handles invalid yaml '$1'": [
    ['./invalid.mode.string.tasks.yml', 'invalid_shell_mode_yaml_string'],
    ['./invalid.mode.number.tasks.yml', 'invalid_shell_mode_yaml_number'],
    async function (testTaskFilePath: string, testFixtureKey: string) {
      const expected = ShellModeFixtures[testFixtureKey]!.substring(1);

      try {
        await new CliSpawnBuilder()
          .taskFile(testTaskFilePath)
          .task('never-run-task')
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

}