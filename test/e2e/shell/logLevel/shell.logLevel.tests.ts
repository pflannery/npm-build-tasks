import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import ShellLogLevelFixtures from './shell.logLevel.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const ShellLogLevelTests = {

  "handles invalid switch '$1'": [
    ['blah', 'invalid_shell_log_level_string_switch'],
    ['0', 'invalid_shell_log_level_number_switch'],
    async function (testLevel: string, fixtureKey: string) {
      const expected = ShellLogLevelFixtures[fixtureKey]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .logLevel(testLevel)
          .task('never-run-task')
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

  "handles valid switch '$1'": [
    [ShellLogLevelNames.step, 'valid_shell_log_level_step'],
    [ShellLogLevelNames.command, 'valid_shell_log_level_command'],
    [ShellLogLevelNames.silent, 'valid_shell_log_level_silent'],
    async function (testLogLevel: string, testFixtureName: string) {
      const expected = ShellLogLevelFixtures[testFixtureName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .logLevel(testLogLevel)
          .task('ok_task')
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "handles invalid yaml '$1'": [
    ['./invalid.level.number.tasks.yml', 'invalid_shell_log_level_yaml_number'],
    ['./invalid.level.string.tasks.yml', 'invalid_shell_log_level_yaml_string'],
    async function (testTaskFilePath: string, testFixtureKey: string) {
      const expected = ShellLogLevelFixtures[testFixtureKey]!.substring(1);

      try {
        await new CliSpawnBuilder()
          .taskFile(testTaskFilePath)
          .task('never-run-task')
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

}