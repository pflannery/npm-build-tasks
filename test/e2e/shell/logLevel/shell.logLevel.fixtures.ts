import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  invalid_shell_log_level_string_switch: `
Invalid log level 'blah'. Valid levels are step|command|error|silent
`,

  invalid_shell_log_level_number_switch: `
Invalid log level '0'. Valid levels are step|command|error|silent
`,

  invalid_shell_log_level_yaml_number: `
Failed to parse ./invalid.level.number.tasks.yml
error: '$.shell.logLevel' => must be equal to 'step|command|error|silent'
`,

  invalid_shell_log_level_yaml_string: `
Failed to parse ./invalid.level.string.tasks.yml
error: '$.shell.logLevel' => must be equal to 'step|command|error|silent'
`,

  valid_shell_log_level_step: `
Executing [ok_task]

1: [ok_task] echo ok
ok

[ok_task] completed 1 task(s)
`,

  valid_shell_log_level_command: `
ok
`,

  valid_shell_log_level_silent: ``,

}