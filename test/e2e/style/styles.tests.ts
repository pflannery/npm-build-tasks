import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import StylesFixtures from './styles.fixtures.js';

const escapesCwd = new ModuleFileLocation(import.meta).join('escapes');

export const StylesTests = {

  "handles invalid style switches": async function () {

    const expected = StylesFixtures.invalid_output_style!.substring(1);
    try {
      await new CliSpawnBuilder()
        .styleStep('invalid')
        .task('some-task')
        .spawn(escapesCwd.toString());

      fail();
    } catch (e: any) {
      equal(e, expected);
    }
  },

  "styles step text using cli switch": async function () {
    const expected = StylesFixtures.style_step_cli_test!.substring(1);
    try {
      const actual = await new CliSpawnBuilder()
        .logLevel(ShellLogLevelNames.step)
        .styleStep('bold')
        .task('style_step_cli_test')
        .spawn(escapesCwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "styles error text using cli switch": async function () {
    const expectedEndsWith = StylesFixtures.style_error_cli_test_ends_with!.substring(1);
    try {
      await new CliSpawnBuilder()
        .logLevel(ShellLogLevelNames.error)
        .styleError('bgRed')
        .task('style_error_cli_test')
        .spawn(escapesCwd.toString());

      fail();
    } catch (actual: any) {
      const actualEndsWith = actual.substring(actual.length - expectedEndsWith.length)
      equal(actualEndsWith, expectedEndsWith);
    }
  },

  "styles step text using tasks file output.styles": async function () {
    const expected = StylesFixtures.style_step_task_file_test!.substring(1);
    try {
      const actual = await new CliSpawnBuilder()
        .logLevel(ShellLogLevelNames.step)
        .task('style_step_task_file_test')
        .spawn(escapesCwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "styles error text using tasks file switch": async function () {
    const expectedEndsWith = StylesFixtures.style_error_task_file_test_ends_with!.substring(1);
    try {
      await new CliSpawnBuilder()
        .logLevel(ShellLogLevelNames.error)
        .task('style_error_task_file_test')
        .spawn(escapesCwd.toString());

      fail();
    } catch (actual: any) {
      const actualEndsWith = actual.substring(actual.length - expectedEndsWith.length)
      equal(actualEndsWith, expectedEndsWith);
    }
  },

}