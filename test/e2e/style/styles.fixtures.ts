import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  style_step_cli_test: `
\x1B[1mExecuting [style_step_cli_test]\x1B[22m

\x1B[1m1: [style_step_cli_test] echo ok\x1B[22m
ok

\x1B[1m[style_step_cli_test] completed 1 task(s)\x1B[22m
`,

  style_error_cli_test_ends_with: `
\x1B[41mExited with error code 1\x1B[49m
\x1B[41m>>>: [style_error_cli_test] failed\x1B[49m
`,

  style_step_task_file_test: `
\x1B[1mExecuting [style_step_task_file_test]\x1B[22m

\x1B[1m1: [style_step_task_file_test] echo ok\x1B[22m
ok

\x1B[1m[style_step_task_file_test] completed 1 task(s)\x1B[22m
`,

  style_error_task_file_test_ends_with: `
\x1B[41mExited with error code 1\x1B[49m
\x1B[41m>>>: [style_error_task_file_test] failed\x1B[49m
`,

  invalid_output_style: `
Invalid style 'invalid'

Valid styles:

bgBlack, bgBlue, bgBlueBright,
bgCyan, bgCyanBright, bgGray,
bgGreen, bgGreenBright, bgMagenta,
bgMagentaBright, bgRed, bgRedBright,
bgWhite, bgWhiteBright, bgYellow,
bgYellowBright, black, blink,
blue, blueBright, bold,
cyan, cyanBright, dim,
doubleunderline, framed, gray,
green, greenBright, hidden,
inverse, italic, magenta,
magentaBright, overlined, red,
redBright, reset, strikethrough,
underline, white, whiteBright,
yellow, yellowBright
`,

}