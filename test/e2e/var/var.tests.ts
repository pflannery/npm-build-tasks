import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import VarFixtures from './var.fixtures.js';

export * from './escaped/var.escaped.tests.js';
export * from './expand/var.expand.tests.js';

const cwd = new ModuleFileLocation(import.meta);

export const VarTests = {

  "[cli switch] parses var name and value": async function () {
    const testTaskName = 'cli_var_switches';
    const expected = VarFixtures[testTaskName]!.substring(1);
    try {
      const actual = await new CliSpawnBuilder()
        .styleDisable()
        .nestedNumbering()
        .var({ A: '"test 1"', B: 'test-2', C: 'test3' })
        .task(testTaskName)
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "[cli switch] handles invalid var names": async function () {
    const expected = VarFixtures['invalid_cli_var_switches']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .var({ _A: 'test1' })
        .task('never_ran_task')
        .spawn(cwd.toString());

      fail();
    } catch (actual: any) {
      equal(actual, expected);
    }
  },

  "executes '$1'": [
    'direct_with_global_var',
    'group_with_global_var',
    'group_direct_with_scope_var',
    'group_task_with_scope_var',
    async function (testTaskName: string) {
      const expected = VarFixtures[testTaskName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}