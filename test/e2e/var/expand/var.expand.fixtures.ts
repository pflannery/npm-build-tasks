import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  test_expand: `
./env/path:./global/path
`,

  test_expand_scope: `
./env/path:./global/path:./scope/path
`,

  test_self_expand: `
./self/path
`,

  test_expand_env_in_var: `
./some/path
`,

  test_self_expand_scope: `
./some/scope/path:./expand/self/path
`

}