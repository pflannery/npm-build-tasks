import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import ExpandVarFixtures from './var.expand.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const ExpandVarTests = {

  "'$1': expands var expressions": [
    'test_expand',
    'test_self_expand',
    'test_expand_scope',
    'test_expand_env_in_var',
    'test_self_expand_scope',
    async (testTaskName: string) => {
      const expected = ExpandVarFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .taskFile('./var.expand.tasks.yml')
          .logLevel(ShellLogLevelNames.command)
          .env({ CLI_ENV: './env/path' })
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}