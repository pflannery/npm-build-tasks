import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  direct_with_global_var_escaped: `
$TEST_GLOBAL_VAR
`,

  group_with_global_var_escaped: `
1 $TEST_GLOBAL_VAR
2 \${TEST_GLOBAL_VAR}
`,

  group_direct_with_scope_var_escaped: `
1 $TEST_SCOPE_VAR
2 \${TEST_SCOPE_VAR}
`,

  group_task_with_scope_var_escaped: `
1 $TEST_CHILD_SCOPE_VAR
2 \${TEST_CHILD_SCOPE_VAR}
3 $TEST_CHILD_SCOPE_VAR
`,

}