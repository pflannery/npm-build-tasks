import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import EscapedVarFixtures from './var.escaped.fixtures.js';
import { ShellLogLevelNames } from '#options';

const cwd = new ModuleFileLocation(import.meta);

export const VarEscapedTests = {

  "executes '$1'": [
    'direct_with_global_var_escaped',
    'group_with_global_var_escaped',
    'group_direct_with_scope_var_escaped',
    'group_task_with_scope_var_escaped',
    async function (testTaskName: string) {
      const expected = EscapedVarFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .logLevel(ShellLogLevelNames.command)
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}