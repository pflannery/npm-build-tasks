import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  cli_var_switches: `
Executing [cli_var_switches]

1: [cli_var_switches] echo $A $B $C
test 1 test-2 test3

[cli_var_switches] completed 1 task(s)
`,

  invalid_cli_var_switches: `
Invalid variable name '_A='
`,

  direct_with_global_var: `
Executing [direct_with_global_var]

1: [direct_with_global_var] echo $TEST_GLOBAL_VAR
global var value

[direct_with_global_var] completed 1 task(s)
`,

  group_with_global_var: `
Executing [group_with_global_var]

1: [group_with_global_var] echo 1 $TEST_GLOBAL_VAR
1 global var value

2: [group_with_global_var] echo 2 $TEST_GLOBAL_VAR
2 global var value

[group_with_global_var] completed 2 task(s)
`,

  group_direct_with_scope_var: `
Executing [group_direct_with_scope_var]

1: [group_direct_with_scope_var] echo 1 $TEST_SCOPE
1 scope var value

2: [group_direct_with_scope_var] echo 2 $TEST_SCOPE
2 scope var value

[group_direct_with_scope_var] completed 2 task(s)
`,

  group_task_with_scope_var: `
Executing [group_task_with_scope_var]

1.1: [child_task_with_scope_var] echo 1 $TEST_CHILD_SCOPE
1 scope parent var value

1.2: [child_task_with_scope_var] echo 2 $TEST_CHILD_SCOPE
2 scope child var value

2: [group_task_with_scope_var] echo 3 $TEST_CHILD_SCOPE
3 scope parent var value

[group_task_with_scope_var] completed 3 task(s)
`,

}