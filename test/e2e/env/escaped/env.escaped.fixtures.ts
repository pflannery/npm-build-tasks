import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  direct_with_global_env_escaped: `
$TEST_GLOBAL
`,

  group_with_global_env_escaped: `
1 $TEST_GLOBAL
2 $TEST_GLOBAL
`,

  group_direct_with_scope_env_escaped: `
1 $TEST_SCOPE
2 $TEST_SCOPE
`,

  group_task_with_scope_env_escaped: `
1 $TEST_CHILD_SCOPE
2 $TEST_CHILD_SCOPE
3 $TEST_CHILD_SCOPE
`,

}