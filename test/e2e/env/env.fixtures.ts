import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  direct_with_global_env: `
Executing [direct_with_global_env]

1: [direct_with_global_env] echo $TEST_GLOBAL
global env value

[direct_with_global_env] completed 1 task(s)
`,

  group_with_global_env: `
Executing [group_with_global_env]

1: [group_with_global_env] echo 1 $TEST_GLOBAL
1 global env value

2: [group_with_global_env] echo 2 $TEST_GLOBAL
2 global env value

[group_with_global_env] completed 2 task(s)
`,

  group_direct_with_scope_env: `
Executing [group_direct_with_scope_env]

1: [group_direct_with_scope_env] echo 1 $TEST_SCOPE
1 scope env value

2: [group_direct_with_scope_env] echo 2 $TEST_SCOPE
2 scope env value

[group_direct_with_scope_env] completed 2 task(s)
`,

  group_task_with_scope_env: `
Executing [group_task_with_scope_env]

1.1: [child_task_with_scope_env] echo 1 $TEST_CHILD_SCOPE
1 parent scope env value

1.2: [child_task_with_scope_env] echo 2 $TEST_CHILD_SCOPE
2 child scope env value

2: [group_task_with_scope_env] echo 3 $TEST_CHILD_SCOPE
3 parent scope env value

[group_task_with_scope_env] completed 3 task(s)
`,

  group_task_with_scope_env_file: `
Executing [group_task_with_scope_env_file]

1: [group_task_with_scope_env_file] echo $A $B $C
test 1 test-2 test3

[group_task_with_scope_env_file] completed 1 task(s)
`,

  missing_scope_env_file: `
Could not find the file './missing.env'
`

}