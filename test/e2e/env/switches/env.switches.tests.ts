import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import EnvSwitchesFixtures from './env.switches.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const EnvSwitchesTests = {

  "[cli switch] parses env name and value": async function () {
    const testTaskName = 'cli_env_switches';
    const expected = EnvSwitchesFixtures[testTaskName]!.substring(1);

    try {
      const actual = await new CliSpawnBuilder()
        .env({ A: '"test 1"', B: 'test-2', C: 'test3' })
        .task(testTaskName)
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "[cli switch] handles invalid env names": async function () {
    const expected = EnvSwitchesFixtures['invalid_cli_env_switches']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .env({ _A: 'test1' })
        .task('never_ran_task')
        .spawn(cwd.toString());

      fail();
    } catch (actual: any) {
      equal(actual, expected);
    }
  },

  "[cli switch] parses env files": async function () {
    const testTaskName = 'cli_env_file_switch';
    const expected = EnvSwitchesFixtures[testTaskName]!.substring(1);

    try {
      const actual = await new CliSpawnBuilder()
        .envFile('./env.txt')
        .task(testTaskName)
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "[cli switch] env switch overrides env file entries": async function () {
    const testTaskName = 'cli_env_file_switch';
    const expected = EnvSwitchesFixtures['cli_env_overrides_env_file']!.substring(1);

    try {
      const actual = await new CliSpawnBuilder()
        .envFile('./env.txt')
        .env({ A: 1, B: true })
        .task(testTaskName)
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

}