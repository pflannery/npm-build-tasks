import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  cli_env_switches: `
Executing [cli_env_switches]

1: [cli_env_switches] echo $A $B $C
test 1 test-2 test3

[cli_env_switches] completed 1 task(s)
`,

  invalid_cli_env_switches: `
Invalid variable name '_A='
`,

  cli_env_file_switch: `
Executing [cli_env_file_switch]

1: [cli_env_file_switch] echo $A $B $C
test 1 test-2 test3

[cli_env_file_switch] completed 1 task(s)
`,

  cli_env_overrides_env_file: `
Executing [cli_env_file_switch]

1: [cli_env_file_switch] echo $A $B $C
1 true test3

[cli_env_file_switch] completed 1 task(s)
`

}