import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  test_expand: `
./expands/test:./test/path
`,

  test_expand_scope: `
./expands/test:./test/path:./scope/path
`,

  test_self_expand: `
./self/path
`,

  test_expand_var_in_env: `
./some/path
`,

  test_self_expand_scope: `
./some/scope/path:./expand/self/path
`

}