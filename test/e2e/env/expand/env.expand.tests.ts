import { ShellLogLevelNames } from '#options';
import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import EnvExpandFixtures from './env.expand.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const EnvExpandTests = {

  "'$1': expands env expressions": [
    'test_expand',
    'test_self_expand',
    'test_expand_scope',
    'test_expand_var_in_env',
    'test_self_expand_scope',
    async (testTaskName: string) => {
      const expected = EnvExpandFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .logLevel(ShellLogLevelNames.command)
          .env({ CLI_ENV: './expands/test' })
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}