import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import EnvFixtures from './env.fixtures.js';

export * from './escaped/env.escaped.tests.js';
export * from './expand/env.expand.tests.js';
export * from './switches/env.switches.tests.js';

const cwd = new ModuleFileLocation(import.meta);

export const EnvTests = {

  "executes '$1'": [
    'direct_with_global_env',
    'group_with_global_env',
    'group_direct_with_scope_env',
    'group_task_with_scope_env',
    'group_task_with_scope_env_file',
    async function (testTaskName: string) {
      const expected = EnvFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "handles missing scope .env file": async function () {
    const expected = EnvFixtures['missing_scope_env_file']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .nestedNumbering()
        .task('missing_scope_env_file')
        .spawn(cwd.toString());

      fail();
    } catch (actual: any) {
      equal(actual, expected);
    }
  },

}