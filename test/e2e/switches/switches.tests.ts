import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import SwitchesFixtures from './switches.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const SwitchesTests = {

  "handles $2 invalid switch": [
    ['-zzz', 'invalid_arg'],
    async function (testSwitch: string, fixtureKey: string) {
      const expected = SwitchesFixtures[fixtureKey]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .styleDisable()
          .nestedNumbering()
          .raw(testSwitch)
          .task('some-task')
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

  "prints version": async function () {
    const expected = 'dev\n';
    try {
      const actual = await new CliSpawnBuilder()
        .version()
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "prints help": async function () {
    const expected = SwitchesFixtures['help']!.substring(1);
    try {
      const actual = await new CliSpawnBuilder()
        .help()
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

}