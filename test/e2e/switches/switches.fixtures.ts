import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  invalid_arg: `
Invalid switch '-zzz'
`,

  help: `
Usage: task [options] <tasknames> [-- args]

Options:
--tasks-file|-tf <path>
  relative path to the tasks yaml file

--env-file|-ef <path>
  relative path to the env file

--env|-e <param=value>
  overrides global env in the tasks file e.g. -e TEST=true -e OTHER=1

--var|-v <param=value>
  overrides global var in the tasks file e.g. -v TEST=true -v OTHER=1

--shell-mode|-sm <mode>
  tty|plain

--log-level|-l <level>
  log level can be: step|command|error|silent

--log-file|-lf <path>
  relative path to the log file

--log-file-level|-lfl <level>
  log level can be: step|command|error|silent

--style-disable|-sd
  disables styles

--style-step|-ss <style>
  style step info e.g. -ss yellowBright -ss bold

--style-error|-se <style>
  style errors e.g. -se red -se bold

--output-nested-steps|-ons
  output nested step numbering e.g. > 1.1

--output-disable-task-time|-odtt
  disable task start time in step output

--list-tasks|-ls
  list available tasks

--version
  shows the current version

`
}