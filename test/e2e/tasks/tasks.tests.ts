import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import TaskFixtures from './tasks.fixtures.js';

export * from './args/tasks.args.tests.js';
export * from './missing/missing.tests.js';
export * from './names/names.tasks.tests.js';
export * from './scripts/scripts.tests.js';

const cwd = new ModuleFileLocation(import.meta);

export const TasksTests = {

  "executes '$1' task": [
    'direct',
    'direct_task',
    'group_direct',
    'group_task',
    async function (testTaskName: string) {
      const expected = TaskFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "executes multiple tasks": [
    [['direct', 'direct_task'], 'multiple_task_direct'],
    async function (testTaskNames: string[], testFixtureName: string) {
      const expected = TaskFixtures[testFixtureName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(...testTaskNames)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}