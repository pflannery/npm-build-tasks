import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  test_task_names: `
Executing [test_task_names]

1.1: [testtask1] echo ok
ok

2.1: [test-task2] echo ok
ok

[test_task_names] completed 2 task(s)
`,

  switch_list_tasks: `
Tasks available:
  test-task2
  test_task_names
  testtask1
`,

  no_args_list_tasks: `
Tasks available:
  test-task2
  test_task_names
  testtask1
`,

  test_task_no_spaces: `
Failed to parse ./space.names.tasks.yml
error: '$.tasks' => 'test spaces' is an invalid property name
`,

  test_task_no_tabs: `
Failed to parse ./tab.names.tasks.yml
error: '$.tasks' => 'test	tabs' is an invalid property name
`

}