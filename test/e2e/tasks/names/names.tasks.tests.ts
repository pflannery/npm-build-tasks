import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import NamesTaskFixtures from './names.tasks.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const NamesTasksTests = {

  "parses task names": async function () {
    const testTaskName = 'test_task_names';
    const expected = NamesTaskFixtures[testTaskName]!.substring(1);

    try {
      const actual = await new CliSpawnBuilder()
        .nestedNumbering()
        .taskFile('./names.tasks.yml')
        .task(testTaskName)
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "-ls switch lists task names": async function () {
    const expected = NamesTaskFixtures.switch_list_tasks!.substring(1);

    try {
      const actual = await new CliSpawnBuilder()
        .taskFile('./names.tasks.yml')
        .listTasks()
        .spawn(cwd.toString());

      equal(actual, expected);
    } catch (e: any) {
      fail(e);
    }
  },

  "case $i: handles invalid task names": [
    ['./space.names.tasks.yml', 'test_task_no_spaces'],
    ['./tab.names.tasks.yml', 'test_task_no_tabs'],
    async function (testFilePath: string, testFixtureKey: string) {
      const expected = NamesTaskFixtures[testFixtureKey]!.substring(1);

      try {
        await new CliSpawnBuilder()
          .taskFile(testFilePath)
          .spawn(cwd.toString());

        fail();
      } catch (actual: any) {
        equal(actual, expected);
      }
    }
  ],

}