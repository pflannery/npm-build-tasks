import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  direct: `
Executing [direct]

1: [direct] echo direct
direct

[direct] completed 1 task(s)
`,

  direct_task: `
Executing [direct_task]

1.1: [direct] echo direct
direct

[direct_task] completed 1 task(s)
`,

  group_direct: `
Executing [group_direct]

1: [group_direct] echo group direct 1
group direct 1

2: [group_direct] echo group direct 2
group direct 2

[group_direct] completed 2 task(s)
`,

  group_task: `
Executing [group_task]

1.1: [direct] echo direct
direct

2.1.1: [direct] echo direct
direct

[group_task] completed 2 task(s)
`,

  direct_task_with_args: `
Executing [direct_task_with_args]

1.1: [direct] echo direct arg1
direct arg1

[direct_task_with_args] completed 1 task(s)
`,

  multiple_task_direct: `
Executing [direct]

1: [direct] echo direct
direct

[direct] completed 1 task(s)

Executing [direct_task]

1.1: [direct] echo direct
direct

[direct_task] completed 1 task(s)
`

}