import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  missing_tasks_file: `
Could not find the file './no.tasks.file.yml'
`,

  missing_task_blank: `
Could not find a task called ''
`,

  missing_task_name: `
Could not find a task called 'some-task'
`,

  missing_tasks_in_tasks_file: `
Failed to parse ./missing.tasks.yml
error: '$' => 'tasks' is required
`,

}