import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import MissingTaskFixtures from './missing.tasks.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const MissingTasksTests = {

  "case $i: handles missing tasks": [
    ['', '', 'missing_task_blank'],
    ['-tf ./no.tasks.file.yml', 'some-task', 'missing_tasks_file'],
    ['-tf ./missing.tasks.yml', 'some-task', 'missing_tasks_in_tasks_file'],
    ['-tf ../tasks.yml', 'some-task', 'missing_task_name'],
    async function (testSwitch: string, testTaskName: string, fixtureKey: string) {
      const expected = MissingTaskFixtures[fixtureKey]!.substring(1);
      try {
        await new CliSpawnBuilder()
          .raw(testSwitch)
          .task(testTaskName)
          .spawn(cwd.toString());

        fail();
      } catch (e: any) {
        equal(e, expected);
      }
    }
  ],

}