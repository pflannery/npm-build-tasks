import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  package_direct: `
Executing [package_direct]

1: [script_direct] npm run -s script_direct
script direct

[package_direct] completed 1 task(s)
`,

  package_direct_with_args: `
Executing [package_direct_with_args]

1: [script_direct] npm run -s script_direct -- arg1
script direct arg1

[package_direct_with_args] completed 1 task(s)
`,

}