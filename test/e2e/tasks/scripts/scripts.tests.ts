import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import PackageFixtures from './scripts.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const PackageTests = {

  "executes '$1' package script": [
    'package_direct',
    'package_direct_with_args',
    async function (testTaskName: string) {
      const expected = PackageFixtures[testTaskName]!.substring(1);

      try {
        const actual = await new CliSpawnBuilder()
          .taskFile('./scripts.tasks.yml')
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}