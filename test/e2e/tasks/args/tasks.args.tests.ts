import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../../cliSpawnBuilder.js';
import TaskArgsFixtures from './tasks.args.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const TasksArgsTests = {

  "executes '$1'": [
    'direct_task_with_args',
    async function (testTaskName: string) {
      const expected = TaskArgsFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

  "passes args from cli": [
    'direct_task_with_cli_args',
    'group_task_with_cli_args',
    async function (testTaskName: string) {
      const expected = TaskArgsFixtures[testTaskName]!.substring(1);
      try {
        const actual = await new CliSpawnBuilder()
          .nestedNumbering()
          .task(testTaskName)
          .taskArgs('123')
          .spawn(cwd.toString());

        equal(actual, expected);
      } catch (e: any) {
        fail(e);
      }
    }
  ],

}