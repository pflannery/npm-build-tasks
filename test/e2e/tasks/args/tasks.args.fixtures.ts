import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  direct_task_with_args: `
Executing [direct_task_with_args]

1.1: [direct] echo direct arg1
direct arg1

[direct_task_with_args] completed 1 task(s)
`,

  direct_task_with_cli_args: `
Executing [direct_task_with_cli_args]

1: [direct_task_with_cli_args] echo cli args
cli args 123

[direct_task_with_cli_args] completed 1 task(s)
`,

  group_task_with_cli_args: `
Executing [group_task_with_cli_args]

1: [group_task_with_cli_args] echo 1
1

2: [group_task_with_cli_args] echo
123

[group_task_with_cli_args] completed 2 task(s)
`

}