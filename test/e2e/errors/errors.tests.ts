import { equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';
import { CliSpawnBuilder } from '../cliSpawnBuilder.js';
import ErrorFixtures from './errors.fixtures.js';

const cwd = new ModuleFileLocation(import.meta);

export const ErrorTests = {

  "handles exit code errors": async function () {
    const expected = ErrorFixtures['error_exit_code']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .styleDisable()
        .nestedNumbering()
        .task('error_exit_code')
        .spawn(cwd.toString());

      fail();
    } catch (e: any) {
      equal(e, expected);
    }
  },

  "handles unexpected errors": async function () {
    const expectedStartsWith = ErrorFixtures['throw_error_starts_with']!.substring(1);
    const expectedEndsWith = ErrorFixtures['throw_error_ends_with']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .styleDisable()
        .nestedNumbering()
        .task('throw_error')
        .spawn(cwd.toString());

      fail();
    } catch (e: any) {
      const actualStartsWith = e.substring(0, expectedStartsWith.length)
      equal(actualStartsWith, expectedStartsWith);

      const actualEndsWith = e.substring(e.length - expectedEndsWith.length)
      equal(actualEndsWith, expectedEndsWith);
    }
  },

  "handles cyclic errors": async function () {

    const expected = ErrorFixtures['extends_cyclic_errors']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .taskFile('./extends/cyclic1.tasks.yml')
        .styleDisable()
        .nestedNumbering()
        .task('dummy')
        .spawn(cwd.toString());

      fail();
    } catch (e: any) {
      equal(e, expected);
    }
  },

  "handles yaml errors": async function () {
    const expected = ErrorFixtures['broken_yaml_errors']!.substring(1);
    try {
      await new CliSpawnBuilder()
        .taskFile('./yaml/broken.tasks.yml')
        .styleDisable()
        .nestedNumbering()
        .task('dummy')
        .spawn(cwd.toString());

      fail();
    } catch (e: any) {
      equal(e, expected);
    }
  },
}