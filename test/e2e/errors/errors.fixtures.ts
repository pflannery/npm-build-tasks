import type { KeyCollection } from '#utils';

export default <KeyCollection<string>>{

  error_exit_code: `
Executing [error_exit_code]

1: [error_exit_code] node -e "process.exit(1)"
Exited with error code 1
>>>: [error_exit_code] failed
`,

  throw_error_starts_with: `
Executing [throw_error]

1: [throw_error] node -e "throw new Error()"
`,

  throw_error_ends_with: `
Exited with error code 1
>>>: [throw_error] failed
`,

  extends_cyclic_errors: `
Cyclic loop detected trying to load './extends/cyclic1.tasks.yml' more than once
Loaded files were: ['./extends/cyclic1.tasks.yml', './extends/cyclic2.tasks.yml']
`,

  broken_yaml_errors: `
Tasks file './yaml/broken.tasks.yml' is invalid
can not read a block mapping entry; a multiline key may not be an implicit key (4:10)

 1 | tasks:
 2 |   test: 
 3 |   echo 123
 4 |   compiie:
--------------^
`,

}