import {
  applyBuiltInEnv,
  createCrossShellCommand,
  createCrossShellPathEnv,
  getPlatformShellInfo
} from '#commands';
import type { KeyCollection } from '#utils';
import { equal, ok } from 'node:assert';
import { delimiter, sep } from 'node:path';

const shellInfo = getPlatformShellInfo();

type TestContext = {
  testCwd: string
  testEnv: KeyCollection<string>
}

export const ShellTests = {

  [applyBuiltInEnv.name]: {

    beforeEach: function (this: TestContext) {
      this.testCwd = process.cwd();
      this.testEnv = {};
    },

    "applies built-in env's": function (this: TestContext) {
      // test
      applyBuiltInEnv(this.testCwd, this.testEnv);

      // assert
      ok(this.testEnv.PATH?.endsWith(`${delimiter}./node_modules/.bin`));
      equal(this.testEnv.PWD, this.testCwd);
    },

    "doesn't overwrite $PWD if already set": function (this: TestContext) {
      const expectedPwd = 'not overwritten';
      this.testEnv.PWD = expectedPwd;

      // test
      applyBuiltInEnv(this.testCwd, this.testEnv);

      // assert
      equal(this.testEnv.PWD, expectedPwd);
    },

  },

  [createCrossShellPathEnv.name]: {

    "ensures cross shell $PATH": () => {
      const expected = `.${sep}path${sep}one${delimiter}.${sep}path${sep}two`;

      // test
      const actual = shellInfo.isWindows
        ? createCrossShellPathEnv('./path/one:./path/two')
        : createCrossShellPathEnv('./path/one;./path/two');

      // assert
      equal(actual, expected);
    },

  },

  [createCrossShellCommand.name]: {

    "replaces cross shell command env": () => {
      const testCommand = 'echo $PATH';

      const expected = shellInfo.isCommandExe
        ? 'echo %PATH%'
        : shellInfo.isPwshExe ? 'echo ${env:PATH}' : testCommand;

      // test
      const actual = createCrossShellCommand(testCommand);

      // assert
      equal(actual, expected);
    }

  }

}