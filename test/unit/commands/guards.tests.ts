import { assertMaskEntries, CommandErrors } from '#commands';
import type { KeyCollection } from '#utils';
import assert, { fail, ok } from 'node:assert';

export const CommandsGuardTests = {

  [assertMaskEntries.name]: {

    "'$1' mask is a valid var": [
      [['VAR1'], { VAR1: 1 }],
      [['VAR1', 'VAR2'], { VAR1: 1, VAR2: 2 }],
      (testMask: string[], vars: KeyCollection<string>) => {
        try {
          // test
          assertMaskEntries(testMask, vars);
          ok(true);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' mask is not a valid var": [
      [[''], {}],
      [[undefined], { VAR1: 1 }],
      [[null], { VAR1: 1 }],
      [[''], { VAR1: 1 }],
      [['VAR_MISSING'], { VAR1: 1 }],
      [['VAR_MISSING'], { VAR1: 1, VAR2: 2 }],
      (testMask: string[], vars: KeyCollection<string>) => {
        try {
          // test
          assertMaskEntries(testMask, vars);
        } catch (error: any) {
          return assert(error instanceof CommandErrors.MaskVariableNotFound);
        }

        fail("guard should throw error");
      }
    ],

  },

}