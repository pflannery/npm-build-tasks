import { ShellLogLevelNames, ShellMode } from '#options';
import { shellOptionsSchema, variableSchema } from '#tasks';
import {
  validateAdditionalProperties,
  validateEnum,
  validatePatternProperties,
  validateProperties,
  validateRequired,
  validateSchema,
  type KeyMap,
  type Schema,
  type SchemaError
} from '#utils';
import { deepEqual, equal } from 'node:assert';

export const SchemaTests = {

  [validateProperties.name]: {

    "case $i: returns true": [
      {
        shell: {
          mode: ShellMode.plain,
          logLevel: ShellLogLevelNames.command
        }
      },
      { shell: { mode: ShellMode.plain } },
      { shell: {} },
      undefined,
      null,
      (testData: any) => {
        const testProps: KeyMap<Schema> = { shell: shellOptionsSchema };
        // test
        const actual = validateProperties(testData, testProps, 'root');
        // assert
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "returns false": () => {
      const testPath = 'root';

      const testData = {
        item1: 123,
        child: { childItem1: 1, additional: 1 }
      };

      const testProps: Schema = {
        type: 'object',
        properties: {
          item1: { type: 'number' },
          child: {
            type: 'object',
            properties: { childItem1: { type: 'number' } },
            required: ['childItem2'],
            additionalProperties: false
          }
        }
      };

      const expected: SchemaError[] = [
        {
          path: `${testPath}.child`,
          message: "cannot have a property called 'additional'"
        },
        {
          path: `${testPath}.child`,
          message: "'childItem2' is required"
        }
      ];

      // test
      const actual = validateProperties(testData, testProps.properties!, testPath);

      // assert
      equal(actual.pass, false);
      deepEqual(actual.errors, expected);
    }

  },

  [validatePatternProperties.name]: {

    "case $i: returns true": [
      { TEST_VAR1: 1 },
      { TEST_VAR1: 1, VAR2: 1 },
      { test_var1: 1 },
      { test_var1: 1, var1: 1 },
      (testData: any) => {
        const testDataProperty = { var: testData };
        const testProps: KeyMap<Schema> = { var: variableSchema };
        // test
        const actual = validatePatternProperties(testDataProperty, testProps, 'root');
        // assert
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "case $i: returns false": [
      [{ '$test': 123 }, "'$test' is an invalid property name"],
      [{ '0test': 123 }, "'0test' is an invalid property name"],
      [{ '_test': 123 }, "'_test' is an invalid property name"],
      [{ '$TEST_VAR1': 123 }, "'$TEST_VAR1' is an invalid property name"],
      [{ '0TEST_VAR2': 123 }, "'0TEST_VAR2' is an invalid property name"],
      [{ '_TEST_VAR2': 123 }, "'_TEST_VAR2' is an invalid property name"],
      (testData: any, expectedFailMsg: string) => {
        const testPath = 'root';
        const testDataProperty = { var: testData };
        const testProps: KeyMap<Schema> = { var: variableSchema };
        const expected: SchemaError[] = [
          {
            path: `${testPath}.var`,
            message: expectedFailMsg
          }
        ];
        // test
        const actual = validatePatternProperties(testDataProperty, testProps, testPath);
        // assert
        equal(actual.pass, false);
        deepEqual(actual.errors, expected);
      }
    ],

  },

  [validateAdditionalProperties.name]: {

    "case $i: returns true": [
      // no additional
      [{ tasks: 1 }, ['tasks'], false],
      // allow additional
      [{ tasks: 1, other: 1 }, ['tasks'], true],
      (testData: any, testKeys: string[], testAdditional: boolean) => {
        const actual = validateAdditionalProperties(testData, testKeys, testAdditional);

        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "case $i: returns false": () => {
      const testData = { tasks: 123 }
      const testAllowKeys = ['items']

      const actual = validateAdditionalProperties(testData, testAllowKeys, false);

      equal(actual.pass, false);
      deepEqual(actual.errors, ["cannot have a property called 'tasks'"]);
    }

  },

  [validateSchema.name]: {

    "returns true for '$2' primitives": [
      [{}, 'object'],
      [[], 'array'],
      ['', 'string'],
      [0, 'number'],
      [true, 'boolean'],

      // multiple types
      [{}, ['string', 'object']],
      ['test', ['string', 'object']],
      [[], ['string', 'array']],
      ['test', ['string', 'array']],
      [true, ['string', 'boolean']],
      ['test', ['string', 'boolean']],
      [0, ['string', 'number']],
      ['test', ['string', 'number']],
      (testData: any, testTypes: string | string[]) => {
        const actual = validateSchema(testData, { type: testTypes }, 'root');
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "returns true for matching array item types": [
      [[], 'array', { type: 'string' }],
      [['test'], 'array', { type: 'string' }],
      [[{}], 'array', { type: 'object' }],
      [['test', {}], 'array', { type: ['string', 'object'] }],
      (testData: any, testSchemaTypes: string | string[], testItems: Schema) => {
        const actual = validateSchema(
          testData,
          {
            type: testSchemaTypes,
            items: testItems
          },
          'root'
        );
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "returns false when data is 'undefined|null' and type is '$1'": [
      ['object', 'must be object'],
      ['string', 'must be string'],
      ['boolean', 'must be boolean'],
      ['number', 'must be number'],
      ['array', 'must be array'],
      (testTypes: string | string[], expectedErrorMsg: string) => {
        const expected: SchemaError[] = [{
          path: 'root',
          message: expectedErrorMsg
        }];

        [undefined, null].forEach(testData => {
          const actual = validateSchema(testData, { type: testTypes }, 'root');
          equal(actual.pass, false)
          deepEqual(actual.errors, expected);
        })
      }
    ],

    "case $1: returns false for mismatch array item types": [
      [[true], 'array', { type: 'string' }, 'root[0]', 'must be string'],
      [[123], 'array', { type: 'string' }, 'root[0]', 'must be string'],
      [['test', 123], 'array', { type: 'string' }, 'root[1]', 'must be string'],
      (
        testData: any,
        testTypes: string | string[],
        testItems: any | any[],
        expectedErrPath: string,
        expectedMessage: string
      ) => {
        const expected: SchemaError[] = [{
          path: expectedErrPath,
          message: expectedMessage
        }];
        const actual = validateSchema(
          testData,
          {
            type: testTypes,
            items: testItems
          },
          'root'
        );

        equal(actual.pass, false);
        deepEqual(actual.errors, expected);
      }
    ],

    "returns true for matching array items schema": () => {
      const testData = [
        'execute command',
        { var: "true" },
        { env: "true" }
      ];

      const testSchema: Schema = {
        type: 'array',
        items: {
          type: ['string', 'object'],
          additionalProperties: false,
          properties: {
            var: { type: ['string'] },
            env: { type: ['string'] }
          }
        },
      }

      const actual = validateSchema(testData, testSchema, 'root');

      equal(actual.pass, true);
      deepEqual(actual.errors, []);
    },

    "returns false for mismatch array items schema": () => {
      const testData = [
        'execute command',
        { other: "true" }
      ];

      const testSchema: Schema = {
        type: 'array',
        items: {
          type: ['string', 'object'],
          additionalProperties: false,
          properties: {
            var: { type: ['string'] },
            env: { type: ['string'] }
          }
        },
      }

      const expected: SchemaError[] = [{
        path: 'root[1]',
        message: "cannot have a property called 'other'"
      }]

      const actual = validateSchema(testData, testSchema, 'root');

      equal(actual.pass, false);
      deepEqual(actual.errors, expected);
    },

  },

  [validateRequired.name]: {

    "case $i: returns true": [
      [{ tasks: 1 }, []],
      [{ tasks: 1 }, ['tasks']],
      [{ tasks: 1, other: 1 }, ['tasks']],
      (testData: any, testRequired: string[]) => {
        const actual = validateRequired(testData, testRequired);
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "case $i: returns false": [
      [{}, ['tasks']],
      [{ other: 1 }, ['tasks']],
      [{ other: 1, test: 1 }, ['tasks']],
      (testData: any, testRequired: string[]) => {
        const actual = validateRequired(testData, testRequired);
        equal(actual.pass, false);
        deepEqual(actual.errors, ["'tasks' is required"]);
      }
    ],

  },

  [validateEnum.name]: {

    "case $i: returns true": [
      [true, [true]],
      [1, [1, 2, 3]],
      [2, [1, 2, 3]],
      [3, [1, 2, 3]],
      ['item1', ['item1', 'item2', 'item3']],
      ['item2', ['item1', 'item2', 'item3']],
      ['item3', ['item1', 'item2', 'item3']],
      (testData: any, testEnum: string[]) => {
        const actual = validateEnum(testData, testEnum);
        equal(actual.pass, true);
        deepEqual(actual.errors, []);
      }
    ],

    "case $i: returns false": [
      [false, [true]],
      ['steps', ['command', 'error']],
      (testData: any, testEnum: string[]) => {
        const actual = validateEnum(testData, testEnum);
        equal(actual.pass, false);
        deepEqual(actual.errors, [`must be equal to '${testEnum.join('|')}'`]);
      }
    ],

  }

}