import { trimEndsWith, trimStartsWith, until } from '#utils';
import { equal } from 'node:assert';

export const StringTests = {

  [trimStartsWith.name]: {
    "case $i: trims chars": [
      ['!"£$%^&*()_+', '!"£$%^&*()_+test', 'test'],
      [')(*&^%$£"!', ')(*&^%$£"!test', 'test'],
      [')(*&^%$£"!', 'test', 'test'],
      (testTrim: string, testText: string, expected: string) => {
        const actual = trimStartsWith(testText, ...testTrim.split(''));
        equal(actual, expected);
      }
    ]
  },

  [trimEndsWith.name]: {
    "case $i: trims chars": [
      [')(*&^%$£"!', 'test!"£$%^&*()', 'test'],
      [')(*&^%$£"!', 'test', 'test'],
      (testTrim: string, testText: string, expected: string) => {
        const actual = trimEndsWith(testText, ...testTrim.split(''));
        equal(actual, expected);
      }
    ]
  },

  [until.name]: {
    "case $i: returns string up until a delimited": [
      ['@', 'test@123', 'test'],
      ['^', 'test@123', undefined],
      (testDelim: string, testText: string, expected: string) => {
        const actual = until(testText, testDelim);
        equal(actual, expected);
      }
    ]
  }

}