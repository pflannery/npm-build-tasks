import { assertFileExists, assertVarName, UtilErrors } from '#utils';
import assert, { fail, ok } from 'assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const testLoc = new ModuleFileLocation(import.meta);
const fixturesLoc = testLoc.join('fixtures');

export const GuardTests = {

  [assertFileExists.name]: {
    "succeeds when file exists": async () => {
      const testRelFilePath = 'env.txt';
      const testCwd = fixturesLoc.join('env').toString();
      try {
        await assertFileExists(testCwd, testRelFilePath);
      } catch (error: any) {
        fail(error);
      }
    },
    "throws when file not exists": async () => {
      const testRelFilePath = 'missing.env.txt';
      const testCwd = fixturesLoc.join('env').toString();
      try {
        await assertFileExists(testCwd, testRelFilePath);
        fail();
      } catch (error: any) {
        ok(error instanceof UtilErrors.FileNotFound)
      }
    }
  },

  [assertVarName.name]: {

    "'$1' is a valid var name": [
      'test',
      'test_name',
      'TEST',
      'TEST_NAME',
      (testVarName: string) => {
        try {
          // test
          assertVarName(testVarName);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' is not a valid var name": [
      undefined,
      null,
      0,
      '0test',
      '_test',
      '.test',
      '0TEST',
      '_TEST',
      '.TEST',
      'TEST ',
      (testVarName: string) => {
        try {
          assertVarName(testVarName);
        } catch (error: any) {
          return assert(error instanceof UtilErrors.InvalidVarName);
        }

        fail("guard should throw error");
      }
    ],

  }

}