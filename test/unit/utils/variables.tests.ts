import {
  castVariablesToString,
  expandDollarExpressions,
  expandVariableExpressions,
  maskValuesWithExpr,
  replaceDollarEnvExprCmd,
  replaceDollarEnvExprPwsh,
  type KeyCollection,
  type KeyMap,
  type VariableCollection
} from '#utils';
import { deepEqual, equal, ok } from 'node:assert';

export const VariablesTests = {

  [replaceDollarEnvExprCmd.name]: {
    "replaces '$1' expressions with '$2'": [
      ['$var1 ${var2}', '%var1% %var2%'],
      ['$var1 ${var2}', '%var1% %var2%'],
      ['\'$var1\' "${var2}"', '\'%var1%\' "%var2%"'],
      ['echo $A $B $C $D', 'echo %A% %B% %C% %D%'],
      ['echo ${A} ${B} ${C} ${D}', 'echo %A% %B% %C% %D%'],
      ['echo "${A}" \'${B}\' "$C" \'$D\'', 'echo "%A%" \'%B%\' "%C%" \'%D%\''],
      // breaks at invalid chars
      ['$PWD/folder', '%PWD%/folder'],
      ['$PWD.folder', '%PWD%.folder'],
      ['$PWD&folder', '%PWD%&folder'],
      // preserves raw
      ['%var1%', '%var1%'],
      (testExpression: string, expected: string) => {
        const actual = replaceDollarEnvExprCmd(testExpression);
        equal(actual, expected);
      }
    ],
    "case $i: ignores espaced $ expressions": [
      ['\\$var1 \\${var2}', '$var1 ${var2}'],
      ['\\${var1} \\$var2', '${var1} $var2'],
      (testExpression: string, expected: string) => {
        const actual = replaceDollarEnvExprCmd(testExpression);
        equal(actual, expected);
      }
    ]
  },

  [replaceDollarEnvExprPwsh.name]: {
    "replaces '$1' expressions with '$2'": [
      ['$var1 ${var2}', '${env:var1} ${env:var2}'],
      ['$var1 ${var2}', '${env:var1} ${env:var2}'],
      ['\'$var1\' "${var2}"', '\'${env:var1}\' "${env:var2}"'],
      ['echo $A $B $C $D', 'echo ${env:A} ${env:B} ${env:C} ${env:D}'],
      ['echo ${A} ${B} ${C} ${D}', 'echo ${env:A} ${env:B} ${env:C} ${env:D}'],
      ['echo "${A}" \'${B}\' "$C" \'$D\'', 'echo "${env:A}" \'${env:B}\' "${env:C}" \'${env:D}\''],
      // breaks at invalid chars
      ['$PWD/folder', '${env:PWD}/folder'],
      ['$PWD.folder', '${env:PWD}.folder'],
      ['$PWD&folder', '${env:PWD}&folder'],
      // preserves original pwsh env statements
      ['$env:var1', '$env:var1'],
      (testExpression: string, expected: string) => {
        const actual = replaceDollarEnvExprPwsh(testExpression);
        equal(actual, expected);
      }
    ],
    "case $i: ignores espaced $ expressions": [
      ['\\$var1 \\${var2}', '`$var1 "`${var2}"'],
      ['\\${var1} \\$var2', '"`${var1}" `$var2'],
      (testExpression: string, expected: string) => {
        const actual = replaceDollarEnvExprPwsh(testExpression);
        equal(actual, expected);
      }
    ]
  },

  [expandVariableExpressions.name]: {
    "replaces '$1' to '$2'": [
      ['echo ${var1}', 'echo 123'],
      ['echo $var1', 'echo 123'],
      ['echo $var1 $var2', 'echo 123 456'],
      ['echo ${var1} ${var2}', 'echo 123 456'],
      // should expand $var2_test not $var2
      ['echo $var2_test', 'echo 789'],
      (testText: string, expected: string) => {
        const vars = {
          var1: '123',
          var2: '456',
          var2_test: '789'
        };

        // test
        const actual = expandVariableExpressions(testText, vars);

        // assert
        equal(actual, expected);
      }
    ],
    "case $i: ignores espaced $ expressions": [
      ['echo \\$var1', 'echo \\$var1'],
      ['echo \\${var1}', 'echo \\${var1}'],
      (testText: string, expected: string) => {
        const vars = {
          var1: '123',
          var2: '456',
          var2_test: '789'
        };
        const actual = expandVariableExpressions(testText, vars);
        equal(actual, expected);
      }
    ]
  },

  [maskValuesWithExpr.name]: {
    "replaces '$1' to '$2'": [
      ['echo ./some/path/from/var', 'echo ${testpath}'],
      ['echo asd23r23fgadgaga', 'echo ${testtoken}'],
      ['echo ./SOME/path/FROM/var', 'echo ${testpath}'],
      ['echo ./some/path/from/var asd23r23fgadgaga', 'echo ${testpath} ${testtoken}'],
      ['echo ./some/path/from/var/ok', 'echo ${testpath}/ok'],
      (testText: string, expected: string) => {
        const vars = {
          testpath: './some/path/from/var',
          testtoken: 'asd23r23fgadgaga'
        };

        // test
        const actual = maskValuesWithExpr(testText, vars);

        // assert
        equal(actual, expected);
      }
    ]
  },

  [expandDollarExpressions.name]: {
    "case $i: replaces $ expressions": [
      [
        { PATH: '$PATH:test/path' },
        { PATH: './path1:./path2' },
        './path1:./path2:test/path'
      ],
      [
        { PATH: '${PATH}:test/path' },
        { PATH: './path1:./path2' },
        './path1:./path2:test/path'
      ],
      (testEnv: KeyMap<string>, testExpr: KeyMap<string>, expected: string) => {
        // test
        const actual = expandDollarExpressions(testEnv, testExpr);

        // assert
        equal(actual.PATH, expected);

        Object.keys(testEnv)
          .every(key => ok(Reflect.has(actual, key)));
      }]
  },

  [castVariablesToString.name]: {
    "case $i: casts vars to string": [
      [{ item: '123' }, { item: '123' }],
      [{ item: true }, { item: 'true' }],
      [{ item: 123 }, { item: '123' }],
      (testVars: VariableCollection, expected: KeyCollection<string>) => {
        // test
        const actual = castVariablesToString(testVars);

        // assert
        deepEqual(actual, expected);
      }
    ]
  },

}