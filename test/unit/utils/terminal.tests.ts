import { shimStyleText } from '#utils';
import { equal } from 'node:assert';
import util from 'node:util';

export const TerminalTests = {

  [shimStyleText.name]: {
    "matches util.styleText when styles are [$2]": [
      ['text', []],
      ['text', ['bold']],
      ['text', ['bold', 'yellow']],
      (testText: string, testStyles: string[]) => {
        // ignore if running older node version < 20.14.*
        if (!util.styleText) return;

        const actual = shimStyleText(testStyles, testText);
        equal(actual, util.styleText(testStyles as any, testText));
      }
    ]
  },

}