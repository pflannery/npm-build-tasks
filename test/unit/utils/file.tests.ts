import { fileExists, readEnvFile, readJsonFile, readYamlFile, UtilErrors } from '#utils';
import { equal, fail, ok } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const testLoc = new ModuleFileLocation(import.meta);
const fixturesLoc = testLoc.join('fixtures');

export const FileTests = {
  [fileExists.name]: {
    "returns true when file exists": async () => {
      const testFilePath = './package.json';
      const testCwd = fixturesLoc.join('json').toString();
      const actual = await fileExists(testCwd, testFilePath);
      ok(actual);
    },
    "returns false when file not exists": async () => {
      const testFilePath = './missing.json';
      const testCwd = fixturesLoc.join('json').toString();
      const actual = await fileExists(testCwd, testFilePath);
      ok(actual === false);
    }
  },
  [readJsonFile.name]: {
    "returns parsed json from a file": async () => {
      const testPackageJsonFile = './package.json';
      const testCwd = fixturesLoc.join('json').toString();
      const test = await readJsonFile(testCwd, testPackageJsonFile)
      equal(test.name, 'smoke')
    },
    "throws InvalidYamlFile when json file has invalid syntax": async () => {
      const testPackageJsonFile = 'invalid.package.json';
      const testCwd = fixturesLoc.join('json').toString();
      try {
        await readJsonFile(testCwd, testPackageJsonFile);
        fail();
      } catch (error: any) {
        ok(error instanceof UtilErrors.InvalidPackageFile)
      }
    }
  },
  [readYamlFile.name]: {
    "returns parsed yaml from a file": async () => {
      const testYamlfilePath = 'tasks.yml';
      const testCwd = fixturesLoc.join('yaml').toString();
      const test = await readYamlFile(testCwd, testYamlfilePath)
      ok(test.tasks)
      ok(Reflect.has(test.tasks, 'direct'))
    },
    "throws InvalidYamlFile when yaml file has invalid syntax": async () => {
      const testYamlfilePath = 'invalid.yaml.yml';
      const testCwd = fixturesLoc.join('yaml').toString();
      try {
        await readYamlFile(testCwd, testYamlfilePath);
        fail();
      } catch (error: any) {
        ok(error instanceof UtilErrors.InvalidYamlFile)
      }
    }
  },
  [readEnvFile.name]: {
    "returns parsed env from a file": async () => {
      const testEnvfilePath = 'env.txt';
      const testCwd = fixturesLoc.join('env').toString();
      const test = await readEnvFile(testCwd, testEnvfilePath);

      equal(test.TEST1, 123)
      equal(test.TEST2, 456)
      equal(test.TEST3, 789)
      equal(test.TEST4, '')
    },
    "throws InvalidEnvFile when env file has invalid syntax": async () => {
      const testEnvfilePath = 'invalid.env.txt';
      const testCwd = fixturesLoc.join('env').toString();
      try {
        await readEnvFile(testCwd, testEnvfilePath);
        fail();
      } catch (error: any) {
        ok(error instanceof UtilErrors.InvalidEnvFile)
      }
    }
  }
}