export default {
  mapWithOneItem: {
    test: {
      ITEM1: ['A', 'B'],
      ITEM2: [1]
    },
    expected: [
      {
        ITEM1: 'A',
        ITEM2: 1
      },
      {
        ITEM1: 'B',
        ITEM2: 1
      },
    ]
  },
  basic: {
    test: {
      ITEM1: ['A', 'B'],
      ITEM2: [1, 2, 3]
    },
    expected: [
      {
        ITEM1: 'A',
        ITEM2: 1
      },
      {
        ITEM1: 'A',
        ITEM2: 2
      },
      {
        ITEM1: 'A',
        ITEM2: 3
      },
      {
        ITEM1: 'B',
        ITEM2: 1
      },
      {
        ITEM1: 'B',
        ITEM2: 2
      },
      {
        ITEM1: 'B',
        ITEM2: 3
      }
    ]
  },
  complex: {
    test: {
      NODE_VERSION: ['iron', 'hydrogen'],
      OS: ['alpine', 'debian'],
      ARCH: ['x86', 'x64']
    },
    expected: [
      {
        NODE_VERSION: 'iron',
        OS: 'alpine',
        ARCH: 'x86',
      },
      {
        NODE_VERSION: 'iron',
        OS: 'alpine',
        ARCH: 'x64',
      },
      {
        NODE_VERSION: 'iron',
        OS: 'debian',
        ARCH: 'x86',
      },
      {
        NODE_VERSION: 'iron',
        OS: 'debian',
        ARCH: 'x64',
      },
      {
        NODE_VERSION: 'hydrogen',
        OS: 'alpine',
        ARCH: 'x86',
      },
      {
        NODE_VERSION: 'hydrogen',
        OS: 'alpine',
        ARCH: 'x64',
      },
      {
        NODE_VERSION: 'hydrogen',
        OS: 'debian',
        ARCH: 'x86',
      },
      {
        NODE_VERSION: 'hydrogen',
        OS: 'debian',
        ARCH: 'x64',
      }
    ]
  }
}
