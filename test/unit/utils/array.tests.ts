import { productMap } from '#utils'
import { deepEqual } from 'assert'
import Fixtures from './fixtures/array/array.fixtures.js'

export const ArrayTests = {

  [productMap.name]: {

    "maps '$1' product from each array": [
      ['mapWithOneItem', Fixtures.mapWithOneItem],
      ['basic', Fixtures.basic],
      ['complex', Fixtures.complex],
      (testTitle: string, testFixture: any) => {
        const actual = productMap(testFixture.test)
        deepEqual(actual, testFixture.expected);
      }
    ],

    "returns empty when an array is empty": () => {
      const testArrayMap = {
        items: ["1", "2", "3"],
        empty: []
      };

      const actual = productMap(testArrayMap)
      deepEqual(actual, []);
    }

  }

}