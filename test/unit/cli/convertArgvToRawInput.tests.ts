import { convertArgvToRawInput } from '#cli';
import { equal } from 'assert';
import { test } from 'mocha-ui-esm';

export const convertArgvToRawInputTests = {

  [test.title]: convertArgvToRawInput.name,

  "joins argv array args into a string": [
    [['-ods'], '-ods'],
    [['-e', 'A='], '-e A=""'],
    [['-e', 'A=test1'], '-e A=test1'],
    [['-e', 'A=word1 word2'], '-e A="word1 word2"'],
    [['task1 task2'], 'task1 task2'],
    [['-e', 'A=word1 word2', 'task1 task2'], '-e A="word1 word2" task1 task2'],
    (testArgs: string[], expected: string) => {
      // test
      const actual = convertArgvToRawInput([
        '',
        '',
        ...testArgs
      ]);

      // assert
      equal(actual, expected);
    }
  ],

}