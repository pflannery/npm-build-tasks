import { CliErrors, parseArgs, } from '#cli';
import {
  getInitialOptions,
  getShellMode,
  ShellLogLevel,
  type Options,
  type OutputOptions,
  type ShellOptions,
  type StyleOptions
} from '#options';
import type { KeyCollection } from '#utils';
import { test } from 'mocha-ui-esm';
import assert, { deepEqual, equal, fail } from 'node:assert';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const cwdLoc = new ModuleFileLocation(import.meta);

export const parseArgTests = {

  [test.title]: parseArgs.name,

  "[tasks-file] switch $1 <path>": [
    '--tasks-file',
    '-tf',
    async (testSwitch: string) => {
      const testPath = './test/other/path/tasks.yml';
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ tasksFilePath: testPath },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(`${testSwitch} ${testPath}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[env-file] switch $1 <path>": [
    '--env-file',
    '-ef',
    async (testSwitch: string) => {
      const testPath = './test/other/path/tasks.yml';
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ envFilePath: testPath },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(`${testSwitch} ${testPath}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[env] switch $1 <env=value>": [
    '--env',
    '-e',
    async (testSwitch: string) => {
      const expectedEnv: KeyCollection<string> = { A: '1', B: 'true', C: 'str', D: '' };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ env: expectedEnv },
        taskNames: ['']
      };

      const testCmdLine = Object.keys(expectedEnv)
        .reduce(
          (acc: string, key: string) => {
            let value = expectedEnv[key];
            value = value?.length === 0 ? "''" : value;
            return acc + `${testSwitch} ${key}=${value} `
          },
          ''
        );

      // test
      const actual = await parseArgs(testCmdLine, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[var] switch $1 <var=value>": [
    '--var',
    '-v',
    async (testSwitch: string) => {
      const expectedVars: KeyCollection<string> = { A: '1', B: 'true', C: 'str', D: '' };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ var: expectedVars },
        taskNames: ['']
      };

      const testCmdLine = Object.keys(expectedVars)
        .reduce(
          (acc: string, key: string) => {
            let value = expectedVars[key];
            value = value?.length === 0 ? "''" : value;
            return acc + `${testSwitch} ${key}=${value} `
          },
          ''
        );

      // test
      const actual = await parseArgs(testCmdLine, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[shell-mode] switch $1 $2": [
    ['--shell-mode', 'tty'],
    ['--shell-mode', 'plain'],
    ['-sm', 'tty'],
    ['-sm', 'plain'],
    async (testSwitch: string, testMode: string) => {
      const expectedShell: Partial<ShellOptions> = {
        ...{ mode: getShellMode(testMode) }
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ shell: expectedShell },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(`${testSwitch} ${testMode}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[style-step] switch $1 <style>": [
    '--style-step',
    '-ss',
    async (testSwitch: string) => {
      const expectedStyles: string[] = ['bold', 'bgRed'];
      const expectedStyle: Partial<StyleOptions> = {
        ...{ step: expectedStyles as any }
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ style: expectedStyle },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(
        `${testSwitch} bold ${testSwitch} bgRed`,
        cwdLoc.toString()
      );

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[style-error] switch $1 <style>": [
    '--style-error',
    '-se',
    async (testSwitch: string) => {
      const expectedStyles: string[] = ['bold', 'bgRed'];
      const expectedStyle: Partial<StyleOptions> = {
        ...{ error: expectedStyles as any }
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ style: expectedStyle },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(
        `${testSwitch} bold ${testSwitch} bgRed`,
        cwdLoc.toString()
      );

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[style-disable] switch $1": [
    '--style-disable',
    '-sd',
    async (testSwitch: string) => {
      const expectedStyle: Partial<StyleOptions> = {
        ...{ disable: true },
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ style: expectedStyle },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(testSwitch, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[output-nested-steps] switch $1": [
    '--output-nested-steps',
    '-ons',
    async (testSwitch: string) => {
      const expectedOutput: Partial<OutputOptions> = {
        ...{ nestedSteps: true },
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ output: expectedOutput },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(testSwitch, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[output-disable-task-time] switch $1": [
    '--output-disable-task-time',
    '-odtt',
    async (testSwitch: string) => {
      const expectedOutput: Partial<OutputOptions> = {
        ...{ disableTaskTime: true },
      };
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ output: expectedOutput },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(testSwitch, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[log-level] switch $1 $2": [
    ['--log-level', 'step', ShellLogLevel.step],
    ['--log-level', 'command', ShellLogLevel.command],
    ['--log-level', 'error', ShellLogLevel.error],
    ['--log-level', 'silent', ShellLogLevel.silent],
    ['-l', 'step', ShellLogLevel.step],
    ['-l', 'command', ShellLogLevel.command],
    ['-l', 'error', ShellLogLevel.error],
    ['-l', 'silent', ShellLogLevel.silent],
    async (testSwitch: string, testLevel: string, expectedLevel: number) => {
      const expectedOptions: Options = {
        ...getInitialOptions(),
        taskNames: ['']
      };

      expectedOptions.shell.logLevel = expectedLevel

      // test
      const actual = await parseArgs(`${testSwitch} ${testLevel}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[log-file] switch $1 <path>": [
    '--log-file',
    '-lf',
    async (testSwitch: string) => {
      const testPath = './test/other/path/output.log';
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ log: { filePath: testPath } },
        taskNames: ['']
      };

      // test
      const actual = await parseArgs(`${testSwitch} ${testPath}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "[log-file-level] switch $1 $2": [
    ['--log-file-level', 'step', ShellLogLevel.step],
    ['--log-file-level', 'command', ShellLogLevel.command],
    ['--log-file-level', 'error', ShellLogLevel.error],
    ['--log-file-level', 'silent', ShellLogLevel.silent],
    ['-lfl', 'step', ShellLogLevel.step],
    ['-lfl', 'command', ShellLogLevel.command],
    ['-lfl', 'error', ShellLogLevel.error],
    ['-lfl', 'silent', ShellLogLevel.silent],
    async (testSwitch: string, testLevel: string, expectedLevel: number) => {
      const expectedOptions: Options = {
        ...getInitialOptions(),
        taskNames: ['']
      };

      expectedOptions.log.logLevel = expectedLevel

      // test
      const actual = await parseArgs(`${testSwitch} ${testLevel}`, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "case $i: [task-name] '$1'": [
    ['task1', ['task1'], ''],
    ['task1 -- arg1', ['task1'], 'arg1'],
    ['task1 task2', ['task1', 'task2'], ''],
    ['task1 task2 -- arg1', ['task1', 'task2'], 'arg1'],
    async (testArgs: string, expectedTaskNames: string[], expectedTaskArgs: string) => {
      const expectedOptions: Options = {
        ...getInitialOptions(),
        ...{ taskNames: expectedTaskNames },
        taskArgs: expectedTaskArgs
      };

      // test
      const actual = await parseArgs(testArgs, cwdLoc.toString());

      // assert
      deepEqual(actual, expectedOptions);
    }
  ],

  "gets taskname from lifecycle env": async () => {
    process.env.npm_lifecycle_event = 'test:lifecycle';

    const expectedOptions: Options = {
      ...getInitialOptions(),
      ...{ taskNames: ['test:lifecycle'] }
    };

    // test
    const actual = await parseArgs('', cwdLoc.toString());

    delete process.env.npm_lifecycle_event;

    // assert
    deepEqual(actual, expectedOptions);
  },

  "invalid switch $1": [
    '--blah',
    '-z',
    async (testSwitch: string) => {
      try {
        await parseArgs(testSwitch, cwdLoc.toString());
      } catch (error) {
        assert(error instanceof CliErrors.InvalidCliSwitch);
        equal(error.message, `Invalid switch '${testSwitch}'`);
        return;
      }

      fail();
    }
  ],

}