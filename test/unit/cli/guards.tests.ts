import { assertShellLogLevel, assertShellMode, CliErrors } from '#cli';
import { ShellLogLevelNames, ShellMode } from '#options';
import assert, { fail, ok } from 'node:assert';

export const CliGuardTests = {

  [assertShellLogLevel.name]: {

    "'$1' is a valid logLevel": [
      ShellLogLevelNames.step,
      ShellLogLevelNames.command,
      ShellLogLevelNames.error,
      ShellLogLevelNames.silent,
      (testLogLevel: string) => {
        try {
          // test
          assertShellLogLevel(testLogLevel);
          ok(true);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' is not a valid logLevel": [
      undefined,
      null,
      '',
      'invalid',
      -1,
      1,
      40,
      (testLogLevel: string) => {
        try {
          // test
          assertShellLogLevel(testLogLevel);
        } catch (error: any) {
          return assert(error instanceof CliErrors.InvalidShellLogLevel);
        }

        fail("guard should throw error");
      }
    ],

  },

  [assertShellMode.name]: {

    "'$1' is a valid shellMode": [
      ShellMode.tty,
      ShellMode.plain,
      (testMode: string) => {
        try {
          // test
          assertShellMode(testMode);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' is not a valid shellMode": [
      undefined,
      null,
      '',
      'invalid',
      0,
      (testMode: string) => {
        try {
          assertShellMode(testMode);
        } catch (error: any) {
          return assert(error instanceof CliErrors.InvalidShellMode);
        }

        fail("guard should throw error");
      }
    ],

  },

}