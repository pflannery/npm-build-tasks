import type { ShellLogger } from '#logging';
import {
  getDefaultOutputOptions,
  getDefaultShellOptions,
  getDefaultStyleOptions
} from '#options';
import {
  parseTask,
  type TaskFlags,
  type TaskProcessContext,
  type TaskProcessContextFlags
} from '#tasks';
import { test } from 'mocha-ui-esm';
import { deepEqual, equal } from 'node:assert';
import { instance, mock } from 'ts-mockito';

type TestContext = {
  testTaskProcessContext: TaskProcessContext
}

export const parseTaskTests = {

  [test.title]: parseTask.name,

  beforeEach: function (this: TestContext) {
    this.testTaskProcessContext = {
      output: getDefaultOutputOptions(),
      shell: getDefaultShellOptions(),
      style: getDefaultStyleOptions(),
      flags: <TaskProcessContextFlags>{
        hasPackageScripts: false,
        isPackageSilent: true
      },
      mask: [],
      logger: instance(mock<ShellLogger>()),
      env: {},
      var: {},
      tasks: {},
      cwd: ''
    };
  },

  "parses args": function (this: TestContext) {
    const testArgs = '345'
    const taskName = 'task-with-args'
    const testExpression = `${taskName} -- ${testArgs}`
    const testContext: TaskProcessContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [taskName]: 'echo ok'
      },
    };

    const actual = parseTask(testExpression, 0, undefined, testContext)

    equal(actual.expression, taskName)
    equal(actual.args, testArgs)
    equal(actual.parent, undefined)
    deepEqual(
      actual.flags,
      <TaskFlags>{
        isSingleTask: true,
        isGroupTask: false,
        isPackageScript: false,
        isPackageSilent: true
      }
    )
  },

  "parses group tasks": function (this: TestContext) {
    const testExpression = 'group-task';
    const testContext: TaskProcessContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testExpression]: [
          'echo 123',
          'echo 456'
        ]
      },
    };

    const actual = parseTask(testExpression, 0, undefined, testContext)
    equal(actual.expression, testExpression)
    equal(actual.parent, undefined)
    deepEqual(
      actual.flags,
      <TaskFlags>{
        isSingleTask: false,
        isGroupTask: true,
        isPackageScript: false,
        isPackageSilent: true
      }
    )
  },

  "parses single tasks": function (this: TestContext) {
    const testExpression = 'single-task';
    const testContext: TaskProcessContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testExpression]: 'echo 123'
      },
    };

    const actual = parseTask(testExpression, 0, undefined, testContext)
    equal(actual.expression, testExpression)
    equal(actual.parent, undefined)
    deepEqual(
      actual.flags,
      <TaskFlags>{
        isSingleTask: true,
        isGroupTask: false,
        isPackageScript: false,
        isPackageSilent: true
      }
    )
  },

  "parses package scripts": function (this: TestContext) {
    const testExpression = 'package-task';
    const testContext: TaskProcessContext = {
      ...this.testTaskProcessContext,
      packageScripts: {
        [testExpression]: 'task'
      },
      flags: <TaskProcessContextFlags>{
        hasPackageScripts: true,
        isPackageSilent: true
      }
    };

    const actual = parseTask(testExpression, 0, undefined, testContext)
    equal(actual.expression, testExpression)
    equal(actual.parent, undefined)
    deepEqual(
      actual.flags,
      <TaskFlags>{
        isSingleTask: false,
        isGroupTask: false,
        isPackageScript: true,
        isPackageSilent: true
      }
    )
  },

}