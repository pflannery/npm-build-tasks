import { FileWriter } from '#logging';
import { getInitialOptions, ShellLogLevel, ShellMode, type Options } from '#options';
import { createTaskProcessContext } from '#tasks';
import { test } from 'mocha-ui-esm';
import { deepEqual, equal, ok } from 'node:assert';
import { unlinkSync } from 'node:fs';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const testLoc = new ModuleFileLocation(import.meta);
const fixturesLoc = testLoc.join('fixtures');
const fixturesPath = fixturesLoc.toString();

type TestContext = {
  options: Options
}

export const createTaskProcessContextTests = {

  [test.title]: createTaskProcessContext.name,

  beforeEach: function (this: TestContext) {
    this.options = getInitialOptions();
  },

  "uses Options from tasks file": {

    "shell.mode '$1'": [
      'tty',
      'plain',
      async function (this: TestContext, testMode: string) {
        this.options.tasksFilePath = fixturesLoc.join(
          `mode/terminal_mode_${testMode}_tasks.yml`
        ).toString();

        const actual = await createTaskProcessContext(
          fixturesPath,
          this.options,
          true
        )

        deepEqual(actual.shell.mode, testMode);
      }
    ],

    "shell.logLevel '$1'": [
      'step',
      'command',
      'error',
      'silent',
      async function (this: TestContext, testLevel: string) {
        this.options.tasksFilePath = fixturesLoc.join(
          `logLevel/log_level_${testLevel}_tasks.yml`
        ).toString();

        const actual = await createTaskProcessContext(
          fixturesPath,
          this.options,
          true
        );

        deepEqual(actual.shell.logLevel, testLevel);
      }
    ],

    "style.step init with empty []": async function (this: TestContext) {
      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.style.step, []);
    },

    "style.error init with ['red']": async function (this: TestContext) {
      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.style.error, ['red']);
    },

    "env": async function (this: TestContext) {
      const expected = {
        TEST_ENV1: 'env 1',
        TEST_ENV2: 'env 2'
      };

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.env, expected)
    },

    "var": async function (this: TestContext) {
      const expected = {
        TEST_VAR1: 'var 1',
        TEST_VAR2: 'var 2'
      };

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.var, expected)
    },

    "mask": async function (this: TestContext) {
      const expected = [
        'TEST_ENV1',
        'TEST_VAR1'
      ];

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.mask, expected)
    },

    "package scripts": async function (this: TestContext) {
      const expected = {
        compile: 'script compile',
        test: 'script test'
      };

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      ok(actual.flags.isPackageSilent)
      ok(actual.flags.hasPackageScripts)
      deepEqual(actual.packageScripts, expected)
    },

    "direct tasks": async function (this: TestContext) {
      const testTaskName = 'direct'
      const expected = 'test direct task'

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      equal(actual.tasks[testTaskName], expected)
    },

    "group tasks": async function (this: TestContext) {
      const testTaskName = 'group'
      const expected = ['test group task 1', 'test group task 2']

      const actual = await createTaskProcessContext(
        fixturesPath,
        this.options,
        true
      );

      deepEqual(actual.tasks[testTaskName], expected)
    },

  },

  "cli options overrides task file options": async function (this: TestContext) {
    const logFilePath = `${fixturesPath}/cli/cli.overrides.log`;
    const expectedOptions: Options = {
      tasksFilePath: './cli/cli.overrides.tasks.yml',
      shell: {
        mode: ShellMode.plain,
        logLevel: ShellLogLevel.command
      },
      log: {
        filePath: logFilePath,
        logLevel: ShellLogLevel.error
      },
      output: {
        nestedSteps: true,
        disableTaskTime: true
      },
      style: {
        disable: false,
        step: ['yellow'],
        error: ['green']
      },
      env: {
        TEST_ENV1: "cli env 1"
      },
      var: {
        TEST_VAR1: "cli env 1"
      },
      taskNames: [],
      taskArgs: '',
    }

    // test
    const actual = await createTaskProcessContext(
      fixturesPath,
      expectedOptions,
      true
    );

    // assert
    const writers = (actual.logger as any).writers[0] as FileWriter;
    deepEqual(actual.shell, expectedOptions.shell);
    deepEqual(actual.output, expectedOptions.output);
    deepEqual(writers.options.log, expectedOptions.log);
    deepEqual(actual.env, expectedOptions.env);
    deepEqual(actual.var, expectedOptions.var);

    // tear down
    await actual.logger.dispose();
    unlinkSync(logFilePath)
  },

}