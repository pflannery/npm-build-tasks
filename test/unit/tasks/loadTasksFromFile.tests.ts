import { getInitialOptions } from '#options';
import { loadTasksFromFile, type TasksFileData } from '#tasks';
import { deepEqual } from 'assert';
import { test } from 'mocha-ui-esm';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const testLoc = new ModuleFileLocation(import.meta);
const fixturesLoc = testLoc.join('fixtures', 'extends');

export const loadTasksFromFileTests = {

  [test.title]: loadTasksFromFile.name,

  "extends a single task file": async function () {
    const testTasksCwd = fixturesLoc.join('single');
    const testCliOpts = getInitialOptions();
    const expectedTaskData: TasksFileData = {
      extends: './single.extends.tasks.yml',
      shell: {
        logLevel: 'error' as any,
        mode: 'plain' as any
      },
      output: {
        nestedSteps: true
      },
      log: {
        filePath: './root.log',
        logLevel: 'command' as any
      },
      style: {
        disable: true,
        step: ['green'],
        error: ['yellow']
      },
      env: { ROOT_ENV: '123', INHERIT_ENV: '456', OVERRIDE_ENV: 'root env' },
      var: { ROOT_VAR: '567', INHERIT_VAR: '789', OVERRIDE_VAR: 'root var' },
      mask: ['INHERIT_VAR', 'ROOT_VAR'],
      tasks: {
        inherited_task: 'echo inherited',
        root_task: 'echo root',
        override_task: 'echo root',
        override_group_task: ['echo root group']
      },
    }

    // test
    const actual = await loadTasksFromFile(
      testTasksCwd.toString(),
      testTasksCwd.join('single.tasks.yml').toString(),
      testCliOpts,
      {}
    );

    // assert
    deepEqual(actual, expectedTaskData);
  },

  "extends from multiple tasks files": async function () {
    const testTasksCwd = fixturesLoc.join('multiple');
    const testCliOpts = getInitialOptions();
    const expectedTaskData: TasksFileData = {
      extends: [
        './extends.2.tasks.yml',
        './extends.1.tasks.yml'
      ],
      shell: {
        logLevel: 'command' as any,
        mode: 'plain' as any
      },
      output: {
        disableTaskTime: false,
        nestedSteps: true
      },
      log: {
        filePath: './root.log',
        logLevel: 'error' as any
      },
      style: {
        disable: true,
        step: ['green'],
        error: ['red']
      },
      env: {
        ROOT_ENV: '123',
        EXTENDS_1_ENV: 'extends env 1',
        EXTENDS_2_ENV: 'extends env 2',
        EXTENDS_ENV_OVERRIDE: 'root env wins'
      },
      var: {
        ROOT_VAR: '567',
        EXTENDS_1_VAR: 'extends var 1',
        EXTENDS_2_VAR: 'extends var 2',
        EXTENDS_VAR_OVERRIDE: 'root var wins'
      },
      mask: ['EXTENDS_1_VAR', 'EXTENDS_2_VAR', 'ROOT_VAR'],
      tasks: {
        extends_1_task: 'echo extends 1 task',
        extends_2_task: 'echo extends 2 task',
        extends_overrides: 'echo extends 2 wins',
        shared_overrides_task: 'echo root wins',
        shared_overrides_group: ['echo root group wins'],
        root_task: 'echo root'
      },
    }

    // test
    const actual = await loadTasksFromFile(
      testTasksCwd.toString(),
      testTasksCwd.join('multiple.tasks.yml').toString(),
      testCliOpts,
      {}
    );

    // assert
    deepEqual(actual, expectedTaskData);
  },
}