import {
  assertFileNotAlreadyLoaded,
  assertStyles,
  assertTaskNameExists,
  TaskErrors,
  type TaskCollection
} from '#tasks';
import assert, { fail, ok } from 'node:assert';

export const TasksGuardTests = {

  [assertTaskNameExists.name]: {

    "'$1' taskname is a valid task": [
      [['clean'], { clean: 1 }],
      [['compile'], { clean: 1, compile: 1 }],
      (taskName: string, testTasks: TaskCollection) => {
        try {
          // test
          assertTaskNameExists(taskName, testTasks);
          ok(true);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' taskname is not a valid task": [
      [[undefined], {}],
      [[null], {}],
      [[''], {}],
      [['clea'], { clean: 1 }],
      [['com'], { clean: 1, compile: 1 }],
      (taskName: string, testTasks: TaskCollection) => {
        try {
          // test
          assertTaskNameExists(taskName, testTasks);
        } catch (error: any) {
          return assert(error instanceof TaskErrors.TaskNameNotExist);
        }

        fail("guard should throw error");
      }
    ],
    
  },

  [assertFileNotAlreadyLoaded.name]: {

    "no error when a tasks file not already loaded": () => {
      const testTasksFilePath = './tasks1.yml';
      const testFilesLoaded = { './tasks2.yml': true };
      try {
        // test
        assertFileNotAlreadyLoaded(testTasksFilePath, testFilesLoaded);
        ok(true);
      } catch (error: any) {
        fail("guard should not error");
      }
    },

    "throws error when a tasks file is already loaded": () => {
      const testTasksFilePath = './tasks.loaded.yml';
      const testFilesLoaded = { './tasks.loaded.yml': true };
      try {
        // test
        assertFileNotAlreadyLoaded(testTasksFilePath, testFilesLoaded);
      } catch (error: any) {
        return assert(error instanceof TaskErrors.ExtendsCyclicLoop);
      }

      fail("guard should throw error");
    }

  },

  [assertStyles.name]: {

    "'$1' includes valid styles": [
      [['bold']],
      [['bold', 'yellow']],
      (testStyles: string[]) => {
        try {
          // test
          assertStyles(testStyles);
          ok(true);
        } catch (error: any) {
          fail("guard should not error");
        }
      }
    ],

    "'$1' includes invalid styles": [
      [[undefined]],
      [[null]],
      [['']],
      [['invalid', 'box']],
      (testStyles: string[]) => {
        try {
          // test
          assertStyles(testStyles);
        } catch (error: any) {
          return assert(error instanceof TaskErrors.InvalidStyle);
        }

        fail("guard should throw error");
      }
    ],

  },

}