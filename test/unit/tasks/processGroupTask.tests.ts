import type { Command } from '#commands';
import type { ShellLogger } from '#logging';
import {
  getDefaultOutputOptions,
  getDefaultShellOptions,
  getDefaultStyleOptions
} from '#options';
import {
  parseTask,
  processGroupTask,
  type GroupTaskScope,
  type TaskProcessContext,
  type TaskProcessContextFlags
} from '#tasks';
import { test } from 'mocha-ui-esm';
import { deepEqual, equal } from 'node:assert';
import { instance, mock } from 'ts-mockito';
import { ModuleFileLocation } from '../../moduleFileLocation.js';

const testLoc = new ModuleFileLocation(import.meta);

type TestContext = {
  testTaskProcessContext: TaskProcessContext
}

export const processGroupTaskTests = {

  [test.title]: processGroupTask.name,

  beforeEach: function (this: TestContext) {
    this.testTaskProcessContext = {
      output: getDefaultOutputOptions(),
      shell: getDefaultShellOptions(),
      style: getDefaultStyleOptions(),
      flags: <TaskProcessContextFlags>{
        hasPackageScripts: false,
        isPackageSilent: true
      },
      mask: [],
      logger: instance(mock<ShellLogger>()),
      env: {},
      var: {},
      tasks: {},
      cwd: ''
    };
  },

  "returns group commands": async function (this: TestContext) {
    const testGroupTaskName = 'group-task';
    const testContextEnv = { 'ENV1': '525' };
    const testContextVar = { 'VAR1': '321' };
    const testCmdLines = [
      'echo 123',
      'echo 678'
    ];

    const testContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testGroupTaskName]: testCmdLines,
      },
      env: testContextEnv,
      var: testContextVar,
    }

    const expected = testCmdLines.map(
      (x, i) => (<Command>{
        name: testGroupTaskName,
        args: '',
        cmd: x,
        env: testContextEnv,
        step: `${i + 1}`,
        repeatIndex: undefined,
        var: testContextVar
      })
    );

    const testTask = parseTask(testGroupTaskName, 0, undefined, testContext);

    // test
    const actual = await processGroupTask(testTask, testContext);

    // assert
    equal(actual.length, expected.length);

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    });
  },

  "sets scoped env for child command(s)": async function (this: TestContext) {
    const testGroupTaskName = 'group-task';
    const testContextEnv = { 'CONTEXT': '987', 'TEST_ENV_EXPAND': './original/path' }
    const testScopeEnvEntry = <GroupTaskScope>{
      env: {
        'SCOPE': '123',
        'TEST_ENV_EXPAND': '$TEST_ENV_EXPAND:./scoped/path'
      }
    };
    const testChildCmdLine1 = 'echo 1';
    const testChildCmdLine2 = 'echo 2';

    const testContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testGroupTaskName]: [
          // set the scoped env
          testScopeEnvEntry,
          // scoped env should be available for following commands
          testChildCmdLine1,
          testChildCmdLine2
        ],
      },
      env: testContextEnv,
    }

    const expectedCmdEnv = {
      'CONTEXT': '987',
      'SCOPE': '123',
      'TEST_ENV_EXPAND': './original/path:./scoped/path'
    };

    const expected = [
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine1,
        env: expectedCmdEnv,
        step: '1',
        repeatIndex: undefined,
        var: {}
      },
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine2,
        env: expectedCmdEnv,
        step: '2',
        repeatIndex: undefined,
        var: {}
      }
    ];

    // parse the task for the test
    const testTask = parseTask(testGroupTaskName, 0, undefined, testContext);

    // test
    const actual = await processGroupTask(testTask, testContext)

    // assert
    equal(actual.length, expected.length);

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    })
  },

  "loads dot env files into scoped env": async function (this: TestContext) {
    const fixturesLoc = testLoc.join('fixtures');
    const testGroupTaskName = 'group-task';
    const testScopeEnvEntry = <GroupTaskScope>{
      'env.file': './env.txt',
      env: {
        'TEST_ENV_FILE_EXPAND': '$TEST_ENV_FILE_EXPAND:./scoped/path'
      }
    };
    const testChildCmdLine1 = 'echo 1';
    const testChildCmdLine2 = 'echo 2';

    const testContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testGroupTaskName]: [
          // set the scoped env
          testScopeEnvEntry,
          // scoped env should be available for following commands
          testChildCmdLine1,
          testChildCmdLine2
        ],
      },
      cwd: fixturesLoc.toString(),
    }

    const expectedCmdEnv = {
      'CONTEXT': '987',
      'TEST_ENV_FILE_EXPAND': './original/path:./scoped/path'
    };

    const expected = [
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine1,
        env: expectedCmdEnv,
        step: '1',
        repeatIndex: undefined,
        var: {}
      },
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine2,
        env: expectedCmdEnv,
        step: '2',
        repeatIndex: undefined,
        var: {}
      }
    ];

    // parse the task for the test
    const testTask = parseTask(testGroupTaskName, 0, undefined, testContext);

    // test
    const actual = await processGroupTask(testTask, testContext)

    // assert
    equal(actual.length, expected.length);

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    })
  },

  "sets scoped var for child command(s)": async function (this: TestContext) {
    const testGroupTaskName = 'group-task';
    const testContextVar = { 'CONTEXT': '987', 'TEST_VAR_EXPAND': './original/path' }
    const testScopeVarEntry = <GroupTaskScope>{
      var: {
        'SCOPE': '123',
        'TEST_VAR_EXPAND': '$TEST_VAR_EXPAND:./scoped/path'
      }
    };
    const testChildCmdLine1 = 'echo 1';
    const testChildCmdLine2 = 'echo 2';

    const testContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testGroupTaskName]: [
          // set the scoped var
          testScopeVarEntry,
          // scoped env should be available for following commands
          testChildCmdLine1,
          testChildCmdLine2
        ],
      },
      var: testContextVar,
    }

    const expectedCmdVar = {
      'CONTEXT': '987',
      'SCOPE': '123',
      'TEST_VAR_EXPAND': './original/path:./scoped/path'
    };

    const expected = [
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine1,
        env: {},
        step: '1',
        repeatIndex: undefined,
        var: expectedCmdVar
      },
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: testChildCmdLine2,
        env: {},
        step: '2',
        repeatIndex: undefined,
        var: expectedCmdVar
      }
    ];

    // parse the task for the test
    const testTask = parseTask(testGroupTaskName, 0, undefined, testContext);

    // test
    const actual = await processGroupTask(testTask, testContext)

    // assert
    equal(actual.length, expected.length);

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    });
  },

  "repeats command(s)": async function (this: TestContext) {
    const testGroupTaskName = 'repeat-task';
    const testChildCmdLine1 = 'echo $TEST';
    const testScopeRepeatEntry = <GroupTaskScope>{
      repeat: {
        'TEST': ['one', 'two', 'three']
      }
    };

    const testContext = {
      ...this.testTaskProcessContext,
      tasks: {
        [testGroupTaskName]: [
          // set the scoped repeat
          testScopeRepeatEntry,
          // should be repeat following commands
          testChildCmdLine1,
        ],
      }
    };

    const expected = [
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: 'echo $TEST',
        env: {},
        var: { TEST: 'one' },
        step: '1',
        repeatIndex: 0,
      },
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: 'echo $TEST',
        env: {},
        var: { TEST: 'two' },
        step: '2',
        repeatIndex: 1
      },
      <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: 'echo $TEST',
        env: {},
        var: { TEST: 'three' },
        step: '3',
        repeatIndex: 2
      }
    ];

    // parse the task for the test
    const testTask = parseTask(testGroupTaskName, 0, undefined, testContext);

    // test
    const actual = await processGroupTask(testTask, testContext)

    // assert
    equal(actual.length, expected.length);

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    });
  },

}