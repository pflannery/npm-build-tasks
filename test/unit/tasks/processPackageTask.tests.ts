import type { Command } from '#commands';
import type { ShellLogger } from '#logging';
import {
  getDefaultOutputOptions,
  getDefaultShellOptions,
  getDefaultStyleOptions
} from '#options';
import {
  parseTask,
  processPackageTask,
  type TaskProcessContext,
  type TaskProcessContextFlags
} from '#tasks';
import { test } from 'mocha-ui-esm';
import { deepEqual } from 'node:assert';
import { instance, mock } from 'ts-mockito';

export const processPackageTaskTests = {

  [test.title]: processPackageTask.name,

  "case $i: returns commands for '$1' package tasks": [
    ['npm', 'testPackageScript', 'testPackageScript', `npm run -s testPackageScript`],
    ['pnpm', 'testPackageScript', 'testPackageScript', `pnpm run -s testPackageScript`],
    ['yarn', 'testPackageScript', 'testPackageScript', `yarn run -s testPackageScript`],
    ['npm', 'testPackageScript -- arg1', 'testPackageScript', `npm run -s testPackageScript -- arg1`],
    ['pnpm', 'testPackageScript -- arg1', 'testPackageScript', `pnpm run -s testPackageScript -- arg1`],
    ['yarn', 'testPackageScript -- arg1', 'testPackageScript', `yarn run -s testPackageScript -- arg1`],
    // package manager defaults 'npm' when not in package.json
    [undefined, 'testPackageScript', 'testPackageScript', `npm run -s testPackageScript`],
    function (testPackageManager: string, testScriptName: string, expectedCmdName: string, expectedCmdLine: string) {
      const testEnv = { 'ENV1': '321' }
      const testVars = { 'VAR1': '321' }
      const testContext: TaskProcessContext = {
        output: getDefaultOutputOptions(),
        shell: getDefaultShellOptions(),
        tasks: {},
        packageManager: testPackageManager,
        packageScripts: {
          [testScriptName]: 'echo test package script'
        },
        style: getDefaultStyleOptions(),
        env: testEnv,
        var: testVars,
        mask: [],
        flags: <TaskProcessContextFlags>{
          hasPackageScripts: true,
          isPackageSilent: true
        },
        logger: instance(mock<ShellLogger>()),
        cwd: ''
      }

      const testTask = parseTask(testScriptName, 1, undefined, testContext);

      const actual = processPackageTask(testTask, testContext)

      const expectedCommands: Command[] = [
        <Command>{
          name: expectedCmdName,
          args: '',
          cmd: expectedCmdLine,
          step: '1',
          repeatIndex: undefined,
          env: testEnv,
          var: testVars
        }
      ]

      actual.forEach((c, i) => {
        deepEqual(c, expectedCommands[i])
      })
    }
  ],

}