import type { Command } from '#commands';
import type { ShellLogger } from '#logging';
import {
  getDefaultOutputOptions,
  getDefaultShellOptions,
  getDefaultStyleOptions
} from '#options';
import {
  parseTask,
  processSingleTask,
  type TaskProcessContext,
  type TaskProcessContextFlags
} from '#tasks';
import { test } from 'mocha-ui-esm';
import { deepEqual } from 'node:assert';
import { instance, mock } from 'ts-mockito';

export const processSingleTaskTests = {

  [test.title]: processSingleTask.name,

  "returns group commands": async function () {
    const testSingleTaskName = 'single-task';
    const testGroupTaskName = 'group-task';
    const expectedCmdLines = [
      'echo 123',
      'echo 678'
    ];

    const testEnv = { 'ENV1': '525' }
    const testVars = { 'VAR1': '321' }
    const testContext: TaskProcessContext = {
      output: getDefaultOutputOptions(),
      shell: getDefaultShellOptions(),
      tasks: {
        [testSingleTaskName]: testGroupTaskName,
        [testGroupTaskName]: expectedCmdLines,
      },
      style: getDefaultStyleOptions(),
      env: testEnv,
      var: testVars,
      mask: [],
      flags: <TaskProcessContextFlags>{
        hasPackageScripts: false,
        isPackageSilent: true
      },
      logger: instance(mock<ShellLogger>()),
      cwd: '',
    }

    const testTask = parseTask(testSingleTaskName, 0, undefined, testContext);

    const actual = await processSingleTask(testTask, testContext)

    const expected = expectedCmdLines.map(
      (x, i) => <Command>{
        name: testGroupTaskName,
        args: '',
        cmd: x,
        step: `1.${i + 1}`,
        repeatIndex: undefined,
        env: testEnv,
        var: testVars
      }
    );

    actual.forEach((c, i) => {
      deepEqual(c, expected[i])
    })
  },

  "returns commands for nested levels": () => console.warn('\ttodo')

}