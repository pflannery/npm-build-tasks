import { CommandErrors, type Command } from '#commands';
import { LoggingErrors, ShellLogger, type IWriter } from '#logging';
import { ShellLogLevel, type OutputOptions } from '#options';
import assert, { equal, fail, ok } from 'node:assert';
import { anyNumber, anyString, instance, mock, verify, when } from 'ts-mockito';

type TestContext = {
  mockWriter: IWriter
  mockOptions: OutputOptions
}

export const ShellLoggerTests = {

  beforeEach: function (this: TestContext) {
    this.mockWriter = mock<IWriter>();
    this.mockOptions = mock<OutputOptions>();
  },

  [ShellLogger.prototype.data.name]: {

    "writes to [Writer].write": function (this: TestContext) {
      const testText = 'data text';
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )

      // test
      log.data(testText, {})

      // verfiy
      verify(this.mockWriter.write(ShellLogLevel.command, testText)).once();
    },

    "writes masked output to [Writer].write": function (this: TestContext) {
      const testText = 'data text some/path 23r23rffsafsdf';
      const testVars = {
        VAR1: 'some/path',
        VAR2: '23r23rffsafsdf'
      };
      const expected = 'data text ${VAR1} ${VAR2}';
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        ['VAR1', 'VAR2'],
        instance(this.mockOptions)
      );

      // test
      log.data(testText, testVars);

      // verfiy
      verify(this.mockWriter.write(ShellLogLevel.command, expected)).once();
    },

  },

  [ShellLogger.prototype.line.name]: {

    "writes to [Writer].writeLine": function (this: TestContext) {
      const testText = 'line text';
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )

      // test
      log.line(testText)

      // verfiy
      verify(this.mockWriter.writeLine(ShellLogLevel.step, testText)).once();
    },

  },

  [ShellLogger.prototype.executing.name]: {

    "writes to [Writer].writeLine": function (this: TestContext) {
      const testTaskName = 'task1';
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )
      const expected = `Executing [${testTaskName}]`;

      // test
      log.executing(testTaskName)

      // verfiy
      verify(this.mockWriter.writeLine(ShellLogLevel.step, expected)).once();
    },

  },

  [ShellLogger.prototype.step.name]: {

    "case $i: writes to [Writer].writeLine": [
      [true, undefined],
      [false, undefined],
      [true, 0],
      [false, 1],
      function (this: TestContext, testDisableTaskTime: boolean, testRepeatIndex: number | undefined) {
        const testCommand: Command = {
          name: 'task name',
          cmd: 'command line',
          step: 'step 1',
          repeatIndex: testRepeatIndex,
          env: {},
          var: {},
          args: ''
        };

        const testRepeatInfo = testRepeatIndex === undefined ? '' : `[${testRepeatIndex}]`;
        const expected = `${testCommand.step}: [${testCommand.name}]${testRepeatInfo} ${testCommand.cmd}`;
        const log = new ShellLogger(
          [instance(this.mockWriter)],
          [],
          instance(this.mockOptions)
        );

        // assert
        when(this.mockOptions.disableTaskTime).thenReturn(testDisableTaskTime);
        when(this.mockWriter.writeLine(ShellLogLevel.step, anyString()))
          .thenCall((actualLevel: number, actualText: string) => {
            // assert log level
            equal(actualLevel, ShellLogLevel.step);

            // assert text starts with the task time
            if (testDisableTaskTime === false) {
              const re = `.+[(]\\d.+?[)]$`;
              ok(RegExp(re).test(actualText));

              // assert text ends with expected message
              const actualStartsWith = actualText.substring(0, expected.length);
              equal(actualStartsWith, expected);
              return;
            }

            equal(actualText, expected);
          });

        // test
        log.step(testCommand.step, testCommand)

        // verify writeline was called
        verify(this.mockWriter.writeLine(anyNumber(), anyString())).once();
      },
    ]
  },

  [ShellLogger.prototype.completed.name]: {

    "case $i: writes to [Writer].writeLine": [
      true,
      false,
      function (this: TestContext, testDisableTaskTime: boolean) {
        const testRan = 2;
        const testName = 'task name';
        const expected = `[${testName}] completed ${testRan} task(s)`;
        const log = new ShellLogger(
          [instance(this.mockWriter)],
          [],
          instance(this.mockOptions)
        );

        // assert
        when(this.mockOptions.disableTaskTime).thenReturn(testDisableTaskTime);
        when(this.mockWriter.writeLine(ShellLogLevel.step, anyString()))
          .thenCall((actualLevel: number, actualText: string) => {
            // assert log level
            equal(actualLevel, ShellLogLevel.step);

            // assert text starts with the task time
            if (testDisableTaskTime === false) {
              const re = `[(]\\d.+?[)]$`;
              ok(RegExp(re).test(actualText));
            }

            // assert text ends with expected message
            const actualStartsWith = actualText.substring(0, expected.length);
            equal(actualStartsWith, expected);
          });

        // test
        log.completed(testName, testRan);

        // verify writeline was called
        verify(this.mockWriter.writeLine(anyNumber(), anyString())).once();
      }
    ],

  },

  [ShellLogger.prototype.fail.name]: {

    "writes to [Writer].writeLine": function (this: TestContext) {
      const testName = 'task name';
      const expected = `>>>: [${testName}] failed`;
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )

      // test
      log.fail(testName)

      // verfiy
      verify(this.mockWriter.writeError(ShellLogLevel.error, expected)).once();
    },

  },

  [ShellLogger.prototype.error.name]: {

    "writes masked process errors to [Writer].writeError": function (this: TestContext) {
      const vars = {
        SECRET: "secret 1",
        SOME_PATH: "./some/path"
      };

      const testError = new Error('some-command -s "secret 1" -s "./some/path" threw an error and printed this');
      const testSource = 'some-command -s "${SECRET}" -s "${SOME_PATH}"';
      const expected = `${testSource} threw an error and printed this`;

      const log = new ShellLogger(
        [instance(this.mockWriter)],
        ['SECRET', 'SOME_PATH'],
        instance(this.mockOptions)
      );

      // test
      log.error(new CommandErrors.ChildProcessError(testError), vars);

      // verfiy
      verify(this.mockWriter.writeError(ShellLogLevel.error, expected)).once();
    },

    "writes exit code errors to [Writer].writeError": function (this: TestContext) {
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )
      const expected = `Exited with error code 100`;

      // test
      log.error(new CommandErrors.ChildProcessExited(100), {});

      // verfiy
      verify(this.mockWriter.writeError(ShellLogLevel.error, expected)).once();
    },

    "throws error for unknown errors": function (this: TestContext) {
      const log = new ShellLogger(
        [instance(this.mockWriter)],
        [],
        instance(this.mockOptions)
      )
      const expected = 'ShellLogger: Unknown log error occurred';

      // test
      try {
        log.error(new Error('unknown'), {});
        fail();
      } catch (e: any) {
        assert(e instanceof LoggingErrors.LogErrorUnknown);
        const actualStartsWith = e.message.substring(0, expected.length)
        equal(actualStartsWith, expected);
      }
    }

  }

}