export * as CliTests from './cli/index.tests.js';
export * as VarUtilsTests from './commands/index.tests.js';
export * as LoggingTests from './logging/shellLogger.tests.js';
export * as TaskTests from './tasks/index.tests.js';
export * as UtilTests from './utils/index.tests.js';