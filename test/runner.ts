import Mocha from 'mocha';
import registerMochaUiEsm from 'mocha-ui-esm';
import SourceMaps from 'source-map-support';
import * as E2eTestModules from './e2e/index.tests.js';
import * as UnitTestModules from './unit/index.tests.js';

// register esm test intergration
registerMochaUiEsm();

// create the test runner
const runner = new Mocha({
  ui: <any>'esm',
  color: true,
  timeout: 60000,
})

// register tests
const testUnit = process.env?.TEST_UNIT === 'true';
const testE2e = process.env?.TEST_E2E === 'true';
const testBoth = !testUnit && !testE2e;
process.env.FORCE_COLOR = "1"

if (testUnit || testBoth) runner.suite.emit('modules', UnitTestModules)
if (testE2e || testBoth) runner.suite.emit('modules', E2eTestModules)

// register soucemap support
SourceMaps.install();

// execute the tests
runner.run(
  failures => {
    if (process) process.exit(failures)
  }
)