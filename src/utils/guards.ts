import { UtilErrors, validVarNameRegExp } from '#utils';
import { access } from 'fs/promises';
import { resolve } from 'path';

export async function assertFileExists(cwd: string, relFilePath: string) {
  try {
    const filePath = resolve(cwd, relFilePath);
    await access(filePath)
  } catch (error: any) {
    throw new UtilErrors.FileNotFound(relFilePath);
  }
}

export function assertVarName(varName: string) {
  if (!varName || validVarNameRegExp.test(varName) === false) {
    throw new UtilErrors.InvalidVarName(varName);
  }
}