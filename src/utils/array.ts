import type { KeyMap } from '#utils';

type ArrayCursor = {
  key: string
  index: number
  length: number
}

export function productMap(arrayMap: KeyMap<Array<string>>) {
  let totalProducts = 1;
  let cursors: ArrayCursor[] = [];

  // init array cursors
  for (const key in arrayMap) {
    totalProducts *= arrayMap[key].length;
    cursors.push({
      key,
      index: 0,
      length: arrayMap[key].length
    });
  }

  let products = 0;
  const results = [];
  while (products < totalProducts) {

    // compose the current product value
    const value: KeyMap<string> = {};
    for (let index = 0; index < cursors.length; index++) {
      const source = cursors[index];
      value[source.key] = arrayMap[source.key][source.index];
    }
    results.push(value);

    // move to the next product
    for (let cursorIndex = 0; cursorIndex < cursors.length; cursorIndex++) {
      const current = cursors[cursorIndex];

      // skip arrays with only one item
      if (current.length === 1) continue;

      // check if we should increment the current index
      let shouldIncrement = true;
      for (let nextIndex = cursorIndex + 1; nextIndex < cursors.length; nextIndex++) {
        const compare = cursors[nextIndex];
        if (compare.index + 1 < compare.length) {
          shouldIncrement = false;
          break;
        }
      }

      // increment the array cursor
      if (shouldIncrement) current.index = (current.index + 1) % current.length;
    }

    products++;
  }

  return results;
}