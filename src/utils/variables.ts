import type { KeyCollection, KeyMap } from '#utils';

export type VariableCollection = KeyMap<boolean | number | string>

export const validVarNameRe = '^[a-zA-Z]+[a-zA-Z_0-9]*$';
export const validVarNameRegExp = new RegExp(validVarNameRe);

const espaceDollar = /\\[$]/g;
const replaceDollarHandleBars = /(?<!\\)[$]{([a-zA-Z0-9_]+)}/g;
const replaceSimpleDollar = /(?<!\\)[$]([a-zA-Z0-9_]+)/g;
export function replaceDollarEnvExprCmd(test: string) {
  return test.replaceAll(replaceDollarHandleBars, '%$1%')
    .replaceAll(replaceSimpleDollar, '%$1%')
    .replaceAll(espaceDollar, '\$');
}

const espaceDollarBracesPwsh = /\\([$]{[a-zA-Z0-9_]+})/g;
const replaceSimpleDollarPwsh = /(?<!\\)[$](?!env:)([a-zA-Z0-9_]+)/g;
export function replaceDollarEnvExprPwsh(test: string) {
  return test.replaceAll(replaceDollarHandleBars, '${env:$1}')
    .replaceAll(replaceSimpleDollarPwsh, '${env:$1}')
    .replaceAll(espaceDollarBracesPwsh, '"`$1"')
    .replaceAll(espaceDollar, '`$');
}

const createVarRegExp = (vn: string): RegExp => new RegExp(`(?<!\\\\)[$](?:{${vn}}|${vn}\\b)`, 'g');
export function expandVariableExpressions(text: string, vars: KeyCollection<string>): string {
  let replaced = text;

  for (const varName in vars) {
    const value = vars[varName];
    if (value === undefined || value === null || value.length === 0) continue;
    const varRegExp = createVarRegExp(varName);
    replaced = replaced.replaceAll(varRegExp, value)
  }

  return replaced;
}

export function expandDollarExpressions(
  expressions: KeyCollection<string>,
  vars: KeyCollection<string>
): KeyCollection<string> {
  // initialize the result with env
  const result: KeyCollection<string> = expressions;

  // for each var, create var regexp
  for (const varName in vars) {
    const varValue = vars[varName] as string;
    const varRegExp = createVarRegExp(varName);

    // for each expr, replace var regexp with the var value
    for (const key in expressions) {
      const value = expressions[key];
      if (value === undefined || value === null || value.length === 0) continue;
      result[key] = value.replaceAll(varRegExp, varValue)
    }
  }

  return result;
}

const escapeRegex = /[.*+?^${}()|[\]\\]/g;
export function maskValuesWithExpr(text: string, vars: KeyCollection<string>): string {
  let output = text;

  try {
    for (const key in vars) {
      const expression = `$\{${key}\}`;
      const value = vars[key];
      if (value === undefined || value === null || value.length === 0) continue;

      const escaped = value.replace(escapeRegex, '\\$&')
      const regex = new RegExp(escaped, 'gi');
      output = output.replaceAll(regex, expression);
    }
  } catch (error: any) {
    // avoid any potential errors thrown by RegExp that would print a var value
    throw new Error(`${maskValuesWithExpr.name} caused an error`)
  }

  return output;
}

export function castVariablesToString(vars: VariableCollection): KeyCollection<string> {
  const result: KeyCollection<string> = {};
  for (const key in vars) {
    result[key] = vars[key].toString();
  }
  return result;
}