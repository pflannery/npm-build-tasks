export const trimStartsWith = (text: string, ...startsWith: string[]) => {
  let trimmed = text;
  for (const match of startsWith) {
    trimmed = trimmed.startsWith(match)
      ? trimmed.substring(match.length)
      : trimmed;
  }
  return trimmed;
}

export const trimEndsWith = (text: string, ...endsWith: string[]) => {
  let trimmed = text;
  for (const match of endsWith) {
    trimmed = trimmed.endsWith(match)
      ? trimmed.substring(0, trimmed.length - match.length)
      : trimmed;
  }
  return trimmed;
}

export const until = (text: string, untilText: string) => {
  if (text) {
    const atPos = text.indexOf(untilText);
    if (atPos > -1) return text.substring(0, atPos);
  }
  return undefined;
}