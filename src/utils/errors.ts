import { EOL } from 'os';

export class FileNotFound extends Error {
  constructor(readonly relativeFilePath: string) {
    super(`Could not find the file '${relativeFilePath}'`);
  }
}

export class InvalidPackageFile extends Error {
  constructor(readonly relFilePath: string, readonly errorMessage: string) {
    super(
      `package.json file '${relFilePath}' is invalid`
      + EOL
      + errorMessage
    );
  }
}

export class InvalidYamlFile extends Error {
  constructor(readonly relFilePath: string, readonly errorMessage: string) {
    super(
      `Tasks file '${relFilePath}' is invalid`
      + EOL
      + errorMessage
    );
  }
}

export class InvalidEnvFile extends Error {
  constructor(readonly relFilePath: string, readonly errorMessage: string) {
    super(
      `env file '${relFilePath}' is invalid`
      + EOL
      + errorMessage
    );
  }
}

export abstract class ErrorWithoutMessage extends Error {
  constructor(readonly unhandled: Error, message: string) {
    // omit the unhandled error.message but keep the stack for diagnostics
    const errorStackOnly = unhandled.stack
      ? EOL + unhandled.stack.replace(unhandled.message, '')
      : '';

    super(message + errorStackOnly);
  }
}

export class CannotCreateDirectory extends Error {
  constructor(readonly path: string, readonly mkdirError: Error) {
    super(`Could not create directory ${path}` + EOL + mkdirError.message);
  }
}

export class InvalidVarName extends Error {
  constructor(readonly varName: string) {
    super(`Invalid variable name '${varName}='`);
  }
}