import type { TasksFileData } from '#tasks';
import {
  assertVarName,
  type KeyCollection,
  type KeyMap,
  trimEndsWith,
  UtilErrors
} from '#utils';
import { load, YAMLException } from 'js-yaml';
import { access, mkdir, readFile } from 'node:fs/promises';
import { dirname, resolve } from 'node:path';

export async function fileExists(cwd: string, relFilePath: string): Promise<boolean> {
  try {
    const filePath = resolve(cwd, relFilePath);
    await access(filePath);
    return true;
  } catch (error: any) {
    return false;
  }
}

export async function readJsonFile(cwd: string, relFilePath: string): Promise<KeyCollection<any>> {
  try {
    const jsonfilePath = resolve(cwd, relFilePath);
    const fileContent = await readFile(jsonfilePath, 'utf8');
    return JSON.parse(fileContent);
  } catch (error: any) {
    if (error instanceof SyntaxError) {
      throw new UtilErrors.InvalidPackageFile(relFilePath, error.message);
    }
    // unhandled error
    throw error;
  }
}

export async function readYamlFile(cwd: string, relFilePath: string): Promise<TasksFileData> {
  try {
    const yamlfilePath = resolve(cwd, relFilePath);
    const yamlContent = await readFile(yamlfilePath, 'utf8');
    return load(yamlContent) as TasksFileData;
  } catch (error: any) {
    if (error instanceof YAMLException) {
      throw new UtilErrors.InvalidYamlFile(relFilePath, error.message);
    }
    // unhandled error
    throw error;
  }
}

export async function readEnvFile(cwd: string, relFilePath: string): Promise<KeyMap<string>> {
  try {
    const envFilePath = resolve(cwd, relFilePath);
    const fileContent = await readFile(envFilePath, 'utf8');
    const lines = fileContent.split('\n');
    const map: KeyMap<string> = {};

    for (const line of lines) {
      const [name, value] = line.split('=');
      assertVarName(name);
      map[name] = value ? trimEndsWith(value, '\r') : '';
    }

    return map;
  } catch (error: any) {
    if (error instanceof UtilErrors.InvalidVarName) {
      throw new UtilErrors.InvalidEnvFile(relFilePath, error.message);
    }
    // unhandled error
    throw error;
  }
}

export async function ensureFolderCreated(cwd: string, filePath: string) {
  const path = dirname(filePath);
  const fullPath = resolve(cwd, dirname(filePath));

  try {
    await access(path);
    return true;
  } catch (e) { }

  try {
    await mkdir(fullPath, { recursive: true });
  } catch (error) {
    throw new UtilErrors.CannotCreateDirectory(path, error as Error);
  }

  return true;
}