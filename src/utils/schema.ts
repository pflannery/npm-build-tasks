// A mini JSON schema parser that only covers what this cli needs
// Avoids v8 loading and compiling 100's of functions from a 3rd party (for cli run perf)
import type { KeyMap } from '#utils';

export type Schema = {
  type: string | string[]
  additionalProperties?: boolean
  required?: string[]
  enum?: string[]
  properties?: KeyMap<Schema>
  patternProperties?: KeyMap<Schema>
  items?: Schema
}

export type ValidateResult = {
  pass: boolean
  errors: string[]
}

export type SchemaError = {
  path: string
  message: string
}

export type SchemaResult = {
  pass: boolean
  errors: SchemaError[]
}

export function validateObjectSchema(data: any, schema: Schema, path: string): SchemaResult {
  const errors: SchemaError[] = [];

  // validate pattern properties
  if (schema.patternProperties) {
    const patternPropsValid = validatePatternProperties(
      data,
      schema.patternProperties,
      path
    );
    errors.push(...patternPropsValid.errors);
    return { pass: errors.length === 0, errors };
  }

  // return if no schema properties
  const schemaProps = schema.properties;
  if (schemaProps === undefined) {
    return { pass: errors.length === 0, errors };
  }

  // validate additional properties
  if (schema.additionalProperties !== undefined) {
    const additionalPropsValid = validateAdditionalProperties(
      data,
      Object.keys(schemaProps),
      schema.additionalProperties
    );

    if (additionalPropsValid.pass === false) {
      errors.push(...mapValidateResult(path, additionalPropsValid));
    }
  }

  // validate required
  if (schema.required instanceof Array) {
    const requiredValid = validateRequired(data, schema.required);
    if (requiredValid.pass === false) {
      errors.push(...mapValidateResult(path, requiredValid));
    }
  }

  // validate properties
  const propsValid = validateProperties(data, schemaProps, path);
  if (propsValid.pass === false) errors.push(...propsValid.errors);

  return { pass: errors.length === 0, errors: errors };
}

export function validateProperties(data: any, properties: KeyMap<Schema>, path: string): SchemaResult {
  const errors: SchemaError[] = [];

  // skip undefined or null data
  if (data === undefined || data === null) return { pass: true, errors };

  // validate properties
  for (const propName in properties) {
    const childData = data[propName];
    const childSchema = properties[propName];

    // skip undefined child data
    if (childData === undefined) continue;

    // assert child schema
    const schemaValid = validateSchema(childData, childSchema, `${path}.${propName}`);
    if (schemaValid.pass === false) errors.push(...schemaValid.errors);
  }

  return { pass: errors.length === 0, errors };
}

const patternCache: { [key: string]: RegExp } = {};
export function validatePatternProperties(data: any, properties: KeyMap<Schema>, path: string): SchemaResult {
  const errors: SchemaError[] = [];

  for (const dataPropName in data) {
    // for each pattern, test the data prop name
    let schemaPropName;
    for (const pattern in properties) {
      const regexp = patternCache[pattern] ?? (patternCache[pattern] = new RegExp(pattern));
      const matched = regexp.test(dataPropName);
      if (matched) {
        schemaPropName = pattern;
        break;
      }
    }

    // skip if no match
    if (!schemaPropName) {
      errors.push({ path, message: `'${dataPropName}' is an invalid property name` });
      continue;
    }

    const childData = data[dataPropName];
    const childSchema = properties[schemaPropName];

    // skip undefined child data
    if (childData === undefined) continue;

    // validate child schema
    const schemaValid = validateSchema(childData, childSchema, `${path}.${dataPropName}`);
    if (schemaValid.pass === false) errors.push(...schemaValid.errors);
  }

  return { pass: errors.length === 0, errors };
}

export function validateAdditionalProperties(
  data: any,
  allowedKeys: string[],
  additionalProperties: boolean
): ValidateResult {
  const errors: string[] = [];
  let pass = true;

  if (additionalProperties) return { pass, errors };

  for (const propName in data) {
    if (allowedKeys.includes(propName) === false) {
      errors.push(`cannot have a property called '${propName}'`);
      pass = false;
    }
  }

  return { pass, errors };
}

export function validateRequired(data: any, required: string[]): ValidateResult {
  const errors: string[] = [];
  let pass = true;

  for (const propName of required) {
    if (!data || Reflect.has(data, propName) === false) {
      errors.push(`'${propName}' is required`);
      pass = false;
    }
  }

  return { pass, errors };
}

export function validateEnum(data: any, allowedValues: string[]): ValidateResult {
  let pass = allowedValues.some(x => x === data);
  const errors: string[] = [];
  if (pass === false) errors.push(`must be equal to '${allowedValues.join('|')}'`);

  return { pass, errors };
}

export function validateSchema(data: any, schema: Schema, path: string = '$'): SchemaResult {
  const type = schema.type;
  const expectedTypes = type instanceof Array ? type : [type];
  let pass = false;
  const errors: SchemaError[] = [];
  for (const type of expectedTypes) {
    switch (type) {
      case 'string':
        pass = data?.constructor === String
        // validate enum
        if (schema.enum instanceof Array) {
          const enumValid = validateEnum(data, schema.enum);
          if (enumValid.pass === false) {
            return { pass: false, errors: mapValidateResult(path, enumValid) };
          }
        }
        break;
      case 'number':
        pass = data?.constructor === Number;
        break;
      case 'boolean':
        pass = data?.constructor === Boolean;
        break;
      case 'object':
        pass = data?.constructor === Object;
        // validate object schema
        if (pass || data === undefined || data === null) {
          const objectValid = validateObjectSchema(data, schema, path);
          if (objectValid.pass === false) {
            return { pass: false, errors: objectValid.errors };
          }
        }
        break;
      case 'array':
        pass = data instanceof Array;
        // validate array items schema
        if (pass && schema.items && data.length > 0) {
          for (let index = 0; index < data.length; index++) {
            const element = data[index];
            const typesValid = validateSchema(element, schema.items, `${path}[${index}]`);
            if (typesValid.pass === false) {
              return { pass: false, errors: typesValid.errors };
            }
          }
        }
        break;
      default: continue;
    }

    if (pass) break;
  }

  if (pass === false) errors.push({
    path,
    message: `must be ${expectedTypes.join('|')}`
  });

  return { pass: errors.length === 0, errors };
}

function mapValidateResult(path: string, result: ValidateResult): SchemaError[] {
  return result.errors.map(x => ({ path, message: x }));
}