import { Style } from '#options';
import { EOL } from 'node:os';
import util from 'node:util';

let { styleText } = util
if (!styleText) styleText = shimStyleText as any;

export const terminalWrite = (text: string, styles: string[] = []): void => {
  const styledText = styleText(<any>styles, text);
  process.stdout.write(styledText);
}

export function terminalWriteLine(text: string = '', styles: string[] = []): void {
  if (text.length > 0 && styles.length > 0)
    terminalWrite(text, styles)
  else
    process.stdout.write(text);

  process.stdout.write(EOL);
}

const escapeStyleCode = (code: number) => `\u001b[${code}m`;

export function shimStyleText(styles: string[], text: string) {
  const prefixes = [];
  const postfixes = [];

  for (const style of styles) {
    const codes = Style[style];
    if (codes && codes.length > 1) {
      prefixes.push(escapeStyleCode(codes[0]));
      postfixes.push(escapeStyleCode(codes[1]));
    }
  }

  return `${prefixes.join('')}${text}${postfixes.reverse().join('')}`;
};