// declare const taskNameSymbol: unique symbol;
// export type TaskName = string & { [taskNameSymbol]: never }
export type KeyMap<T> = { [key: string]: T }

export type KeyCollection<T> = KeyMap<T | undefined>;