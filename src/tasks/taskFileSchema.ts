import { ShellLogLevel, ShellMode } from '#options';
import { validVarNameRe, type Schema } from '#utils';

const validTaskNameRegExp = String.raw`^\S+$`;

const styleOptionsSchema: Schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    disable: { type: 'boolean' },
    step: { type: ['array'], items: { type: 'string' } },
    error: { type: ['array'], items: { type: 'string' } }
  }
};

const outputOptionsSchema: Schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    nestedSteps: { type: 'boolean' },
    disableTaskTime: { type: 'boolean' }
  }
};

export const shellOptionsSchema: Schema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    mode: { type: 'string', enum: Object.keys(ShellMode) },
    logLevel: { type: ['string', 'number'], enum: Object.keys(ShellLogLevel) }
  }
};

const logOptionsSchema = {
  type: 'object',
  additionalProperties: false,
  properties: {
    filePath: { type: 'string' },
    logLevel: { type: ['string', 'number'], enum: Object.keys(ShellLogLevel) }
  }
};

export const variableSchema = {
  type: 'object',
  patternProperties: {
    [validVarNameRe]: {
      type: ['string', 'boolean', 'number'],
    },
  }
};

export const repeatSchema = {
  type: 'object',
  patternProperties: {
    [validVarNameRe]: {
      type: 'array',
      items: {
        type: ['boolean', 'string', 'number']
      }
    },
  }
};

export const getTaskFileDataSchema = (): Schema => ({
  type: 'object',
  additionalProperties: false,
  properties: {
    extends: {
      type: ['string', 'array'],
      items: { "type": "string" }
    },
    shell: shellOptionsSchema,
    log: logOptionsSchema,
    output: outputOptionsSchema,
    style: styleOptionsSchema,
    env: variableSchema,
    var: variableSchema,
    mask: {
      type: 'array',
      items: { type: "string" }
    },
    tasks: {
      type: 'object',
      patternProperties: {
        [validTaskNameRegExp]: {
          type: ['string', 'array'],
          items: {
            type: ['string', 'object'],
            additionalProperties: false,
            properties: {
              'env.file': { type: 'string' },
              env: variableSchema,
              var: variableSchema,
              repeat: repeatSchema
            }
          },
        }
      }
    },
  },
  required: ['tasks'],
});