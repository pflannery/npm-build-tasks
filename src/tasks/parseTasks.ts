import type { Command } from '#commands';
import {
  parseTask,
  processDirectTask,
  processGroupTask,
  processPackageTask,
  processSingleTask,
  type Task,
  type TaskProcessContext
} from '#tasks';

export async function parseTasks(
  expression: string,
  index: number,
  parent: Task | undefined,
  context: TaskProcessContext
): Promise<Array<Command>> {
  const task = parseTask(expression, index, parent, context);

  if (task.flags.isGroupTask) return await processGroupTask(task, context);

  if (task.flags.isSingleTask) return await processSingleTask(task, context);

  if (task.flags.isPackageScript) return processPackageTask(task, context);

  return processDirectTask(task, context)
}