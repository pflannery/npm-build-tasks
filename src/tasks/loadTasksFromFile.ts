import type { Options } from '#options';
import {
  assertFileNotAlreadyLoaded,
  assertTaskFileSchema,
  type TasksFileData
} from '#tasks';
import { assertFileExists, readYamlFile, type KeyMap } from '#utils';

export async function loadTasksFromFile(
  cwd: string,
  tasksFilePath: string,
  cliOpts: Options,
  loadedFiles: KeyMap<boolean>
): Promise<TasksFileData> {
  // assert file not already loaded
  assertFileNotAlreadyLoaded(tasksFilePath, loadedFiles);

  // read yaml file and assert schema
  await assertFileExists(cwd, tasksFilePath);
  const taskFileData = await readYamlFile(cwd, tasksFilePath);
  assertTaskFileSchema(tasksFilePath, taskFileData);

  // record the file as loaded
  loadedFiles[tasksFilePath] = true;

  // return file data when extends is not set
  if (!taskFileData.extends) return taskFileData;

  // load and merge extendable
  const extendFilePaths = taskFileData.extends instanceof Array
    // reverse to make the last extends override all other extends
    ? taskFileData.extends.reverse()
    : [taskFileData.extends];

  let mergedTaskData: TasksFileData = taskFileData;
  for (const extendFilePath of extendFilePaths) {
    const extendsTaskData = await loadTasksFromFile(
      cwd,
      extendFilePath,
      cliOpts,
      loadedFiles
    );

    mergedTaskData = {
      extends: taskFileData.extends,
      tasks: { ...extendsTaskData.tasks, ...mergedTaskData.tasks },
      shell: { ...extendsTaskData.shell, ...mergedTaskData.shell },
      log: { ...extendsTaskData.log, ...mergedTaskData.log },
      output: { ...extendsTaskData.output, ...mergedTaskData.output },
      style: { ...extendsTaskData.style, ...mergedTaskData.style },
      env: { ...extendsTaskData.env, ...mergedTaskData.env },
      var: { ...extendsTaskData.var, ...mergedTaskData.var },
      mask: [...extendsTaskData.mask ?? [], ...mergedTaskData.mask ?? []],
    };
  }

  return mergedTaskData;
}