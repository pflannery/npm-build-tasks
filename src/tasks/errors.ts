import { getPrintedStyles } from '#options';
import { type KeyMap } from '#utils';
import { EOL } from 'os';

export class TaskNameNotExist extends Error {
  constructor(readonly taskName: string) {
    super(`Could not find a task called '${taskName}'`);
  }
}

export class ExtendsCyclicLoop extends Error {
  constructor(readonly taskFilePath: string, readonly filesLoaded: KeyMap<boolean>) {
    super(
      `Cyclic loop detected trying to load '${taskFilePath}' more than once`
      + EOL
      + `Loaded files were: ['${Object.keys(filesLoaded).join('\', \'')}']`
    );
  }
}

export class InvalidStyle extends Error {
  constructor(readonly style: string) {
    super(
      `Invalid style '${style}'`
      + EOL + EOL
      + 'Valid styles:'
      + EOL + EOL
      + getPrintedStyles()
    );
  }
}

export class InvalidTaskFileSchema extends Error {
  constructor(readonly taskFilePath: string, readonly schemaErrors: string[]) {
    super(
      `Failed to parse ${taskFilePath}`
      + EOL
      + `${schemaErrors.join(EOL)}`
    );
  }
}