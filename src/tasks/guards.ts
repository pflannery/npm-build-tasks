import { Style } from '#options';
import {
  getTaskFileDataSchema,
  TaskErrors,
  type TaskCollection,
  type TasksFileData
} from '#tasks';
import { validateSchema, type KeyMap } from '#utils';

export function assertTaskNamesExists(taskNames: string[], tasks: TaskCollection) {
  for (const taskName of taskNames) {
    assertTaskNameExists(taskName, tasks);
  }
}

export function assertTaskNameExists(taskName: string, tasks: TaskCollection) {
  if (Reflect.has(tasks, taskName) === false) {
    throw new TaskErrors.TaskNameNotExist(taskName);
  }
}

export function assertFileNotAlreadyLoaded(tasksRelFilePath: string, filesLoaded: KeyMap<boolean>) {
  if (filesLoaded[tasksRelFilePath] === true) {
    throw new TaskErrors.ExtendsCyclicLoop(tasksRelFilePath, filesLoaded);
  }
}

export function assertStyles(testStyles: string[]) {
  const allowedStyles = Object.keys(Style);
  for (const style of testStyles) {
    if (allowedStyles.includes(style) === false) {
      throw new TaskErrors.InvalidStyle(style);
    }
  }
}

export function assertTaskFileSchema(taskFilePath: string, taskFileData: TasksFileData) {
  const valid = validateSchema(taskFileData, getTaskFileDataSchema() as any);
  if (valid.pass === false) {
    const errors: string[] = [];
    for (const error of valid.errors) {
      errors.push(`error: '${error.path}' => ${error.message}`);
    }

    throw new TaskErrors.InvalidTaskFileSchema(taskFilePath, errors);
  }
}