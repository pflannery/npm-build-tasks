import type { Command } from '#commands';
import type { ShellLogger } from '#logging';
import type { LogFileOptions, OutputOptions, ShellOptions, StyleOptions } from '#options';
import type { KeyCollection, KeyMap, VariableCollection } from '#utils';

export type TaskFlags = {
  isGroupTask: boolean
  isSingleTask: boolean
  isPackageScript: boolean
  isPackageSilent: boolean
}

export type Task = {
  expression: string
  parent: Task | undefined
  args: string
  index: number
  repeatIndex: number | undefined
  step: string
  flags: TaskFlags
}

export type GroupTaskScope = {
  'env.file'?: string
  env?: VariableCollection
  var?: VariableCollection
  repeat?: KeyMap<Array<string>>
}

export type TaskCollection = { [key: string]: string | Array<string | GroupTaskScope> }

export type TasksFileData = {
  extends?: string | string[]
  tasks: TaskCollection
  shell: ShellOptions
  log: LogFileOptions
  output: OutputOptions
  style: StyleOptions
  env: VariableCollection
  var: VariableCollection
  mask: string[]
}

export type TaskProcessContextFlags = {
  hasPackageScripts: boolean
  isPackageSilent: boolean
}

export type TaskProcessContext = {
  flags: TaskProcessContextFlags
  tasks: TaskCollection
  cwd: string
  packageManager?: string
  packageScripts?: KeyCollection<string>
  logger: ShellLogger
  shell: ShellOptions
  output: OutputOptions
  style: StyleOptions
  env: KeyCollection<string>
  var: KeyCollection<string>
  mask: string[]
}

export type TaskProcessor = (task: Task, context: TaskProcessContext) => Array<Command>;