import { FileWriter, ProcessWriter, ShellLogger, type IWriter } from '#logging';
import { getDefaultOptions, ShellMode, type Options } from '#options';
import { assertStyles, loadTasksFromFile, type TaskProcessContext } from '#tasks';
import {
  assertFileExists,
  castVariablesToString,
  ensureFolderCreated,
  fileExists,
  readEnvFile,
  readJsonFile,
  until
} from '#utils';

export async function createTaskProcessContext(
  cwd: string,
  cliOpts: Options,
  isPackageSilent: boolean = true
): Promise<TaskProcessContext> {
  const taskFileData = await loadTasksFromFile(
    cwd,
    cliOpts.tasksFilePath,
    cliOpts,
    {}
  );

  // load env file data
  let envFileData = {};
  if (cliOpts.envFilePath) {
    await assertFileExists(cwd, cliOpts.envFilePath);
    envFileData = await readEnvFile(cwd, cliOpts.envFilePath);
  }

  // merge cli opts with tasks file opts otherwise fallback to defaults
  const defaulOptions = getDefaultOptions();
  const mergedOptions: Options = {
    tasksFilePath: cliOpts.tasksFilePath,
    taskNames: cliOpts.taskNames ?? defaulOptions.taskNames,
    taskArgs: '',
    shell: { ...defaulOptions.shell, ...taskFileData.shell, ...cliOpts.shell },
    log: { ...defaulOptions.log, ...taskFileData.log, ...cliOpts.log },
    output: { ...defaulOptions.output, ...taskFileData.output, ...cliOpts.output },
    style: { ...defaulOptions.style, ...taskFileData.style, ...cliOpts.style },
    env: { ...taskFileData.env, ...envFileData, ...cliOpts.env },
    var: { ...taskFileData.var, ...cliOpts.var },
  };

  // reset styles if disabled
  if (mergedOptions.style.disable) {
    mergedOptions.style.step = [];
    mergedOptions.style.error = [];
  }

  // assert styles
  assertStyles(mergedOptions.style.step);
  assertStyles(mergedOptions.style.error);

  // check for log file
  const hasLogFile = mergedOptions.log.filePath.length > 0;

  // ensure log file dir(s) are created
  if (hasLogFile) await ensureFolderCreated(cwd, mergedOptions.log.filePath);

  // enforce 'plain' shell mode
  const mask = taskFileData.mask ?? [];
  if (hasLogFile || mask.length > 0) mergedOptions.shell.mode = ShellMode.plain;

  // get the package.json
  const packageJsonFile = './package.json';
  const packageExists = await fileExists(cwd, packageJsonFile)
  let packageJson, packageScripts, packageManager;
  if (packageExists) {
    packageJson = await readJsonFile(cwd, packageJsonFile);
    packageScripts = packageJson.scripts;
    packageManager = until(packageJson.packageManager, '@');
  }

  // setup shell logging
  const writers: IWriter[] = [new ProcessWriter(mergedOptions)];
  if (hasLogFile) writers.push(new FileWriter(mergedOptions));

  return {
    flags: {
      hasPackageScripts: !!packageScripts,
      isPackageSilent
    },
    tasks: taskFileData.tasks,
    cwd,
    packageManager,
    packageScripts,
    logger: new ShellLogger(writers, mask, mergedOptions.output),
    shell: mergedOptions.shell,
    output: mergedOptions.output,
    style: mergedOptions.style,
    env: castVariablesToString(mergedOptions.env),
    var: castVariablesToString(mergedOptions.var),
    mask
  };
}