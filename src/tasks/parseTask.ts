import type { Task, TaskProcessContext } from '#tasks';

export function parseTask(
  expression: string,
  index: number,
  parent: Task | undefined,
  context: TaskProcessContext
): Task {
  let parsedExpression = expression.trim();
  const argsPos = parsedExpression.indexOf(' -- ');
  const hasArgs = argsPos > -1;

  let args = '';
  if (hasArgs) {
    args = parsedExpression.substring(argsPos + 4).trim();
    parsedExpression = parsedExpression.substring(0, argsPos).trim();
  }

  const isTask = Reflect.has(context.tasks, parsedExpression);
  const isGroupTask = isTask && context.tasks[parsedExpression] instanceof Array;
  const isSingleTask = isTask && isGroupTask === false;
  const isPackageScript = isTask === false
    && context.flags.hasPackageScripts
    // context.packageScripts is defined here!
    && Reflect.has(context.packageScripts!, parsedExpression);

  return {
    expression: parsedExpression,
    parent,
    args,
    index,
    step: parent
      ? parent.index !== 0
        ? `${parent.step}.${index}`
        : `${index}`
      : `${index}`,
    repeatIndex: parent?.repeatIndex,
    flags: {
      isSingleTask,
      isGroupTask,
      isPackageScript,
      isPackageSilent: context.flags.isPackageSilent
    }
  };
}