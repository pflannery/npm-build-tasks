import { type Command } from '#commands';
import type { Task, TaskProcessContext } from '#tasks';

export function processDirectTask(task: Task, context: TaskProcessContext): Array<Command> {
  const command: Command = {
    name: task.parent!.expression,
    cmd: task.expression,
    args: task.args,
    step: task.step,
    repeatIndex: task.repeatIndex,
    var: context.var,
    env: context.env
  };

  return [command];
}