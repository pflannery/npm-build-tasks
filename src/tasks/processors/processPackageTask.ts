import { type Command } from '#commands';
import type { Task, TaskProcessContext } from '#tasks';

export function processPackageTask(task: Task, context: TaskProcessContext): Array<Command> {
  const manager = context.packageManager ?? 'npm';
  const silentSwitch = context.flags.isPackageSilent ? '-s ' : '';
  const args = task.args.length > 0
    ? ` -- ${task.args}`
    : '';

  const command: Command = {
    name: task.expression,
    cmd: `${manager} run ${silentSwitch}${task.expression}${args}`,
    args: '',
    step: task.step,
    repeatIndex: task.repeatIndex,
    var: context.var,
    env: context.env
  };

  return [command];
}