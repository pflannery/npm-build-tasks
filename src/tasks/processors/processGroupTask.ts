import { type Command } from '#commands';
import {
  parseTasks,
  type GroupTaskScope,
  type Task,
  type TaskProcessContext
} from '#tasks';
import {
  assertFileExists,
  castVariablesToString,
  expandDollarExpressions,
  productMap,
  readEnvFile,
  type KeyCollection,
  type VariableCollection
} from '#utils';

export async function processGroupTask(task: Task, context: TaskProcessContext): Promise<Command[]> {
  const group = context.tasks[task.expression] as Array<string | GroupTaskScope>;
  const scopedContext: TaskProcessContext = { ...context };
  const childCmds = await processGroup(group, task, scopedContext, 0);
  return childCmds.flat();
}

async function processGroup(
  group: Array<string | GroupTaskScope>,
  task: Task,
  scopedContext: TaskProcessContext,
  childTaskIndex: number
): Promise<Command[][]> {
  const childCmds: Command[][] = [];

  for (let index = 0; index < group.length; index++) {
    const expression = group[index];

    if (typeof expression === 'string') {
      const childTasks = await parseTasks(
        expression as string,
        ++childTaskIndex,
        task,
        scopedContext
      );
      childCmds.push(childTasks);
      continue;
    }

    // load env file into the scope context
    const dotEnvFile = expression['env.file'];
    if (dotEnvFile) {
      await assertFileExists(scopedContext.cwd, dotEnvFile);
      const envFileData = await readEnvFile(scopedContext.cwd, dotEnvFile)
      scopedContext.env = { ...scopedContext.env, ...envFileData };
    }

    if (expression.var) {
      // expand scope var with context var
      scopedContext.var = expand(expression.var, scopedContext.var);
      // expand self
      scopedContext.var = expandDollarExpressions(scopedContext.var, scopedContext.var);
    }

    // expand scope env with global env
    if (expression.env) scopedContext.env = expand(expression.env, scopedContext.env);

    // expand var into env
    scopedContext.env = expandDollarExpressions(scopedContext.env, scopedContext.var);

    // repeat commands after the repeat statement
    if (expression.repeat) {
      const repeatGroup = group.slice(index + 1);
      const repeat = productMap(expression.repeat);
      for (let repeatIndex = 0; repeatIndex < repeat.length; repeatIndex++) {
        const repeatVars = repeat[repeatIndex];
        const repeatTask = { ...task, repeatIndex };

        scopedContext.var = expand(repeatVars, scopedContext.var);

        const repeatCmds = await processGroup(
          repeatGroup,
          repeatTask,
          scopedContext,
          childTaskIndex++
        );

        childCmds.push(...repeatCmds);
      }

      break;
    }
  }

  return childCmds;
}

function expand(values: VariableCollection, expressions: KeyCollection<string>) {
  const castValues = castVariablesToString(values);
  const expandedValues = expandDollarExpressions(castValues, expressions);
  return { ...expressions, ...expandedValues };
}