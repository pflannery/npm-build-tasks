import type { Command } from '#commands';
import {
  assertTaskNameExists,
  parseTasks,
  type Task,
  type TaskProcessContext
} from '#tasks';

export async function processSingleTask(task: Task, context: TaskProcessContext): Promise<Array<Command>> {
  assertTaskNameExists(task.expression, context.tasks);

  const childTask = context.tasks[task.expression] as string;
  const childTaskExpression = `${childTask} ${task.args}`;
  const childCmds = await parseTasks(
    childTaskExpression,
    1,
    task,
    context
  );
  return childCmds.flat();
}