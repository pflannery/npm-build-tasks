import {
  applyBuiltInEnv,
  assertMaskEntries,
  createCrossShellCommand,
  createCrossShellPathEnv,
  spawnProcess,
  Stdio,
  type Command,
  type ProcessOptions
} from '#commands';
import { ShellLogLevel, ShellMode } from '#options';
import { parseTasks, type TaskProcessContext } from '#tasks';
import { expandDollarExpressions, expandVariableExpressions } from '#utils';

export async function executeCommands(
  taskName: string,
  taskArgs: string,
  cwd: string,
  context: TaskProcessContext
) {
  // parse tasks into commands
  const commands = await parseTasks(taskName, 0, undefined, context);

  // delete npm_lifecycle_event to prevent infinte loops via ' -- ' npm-run commands
  const env = { ...process.env };
  delete env.npm_lifecycle_event;
  applyBuiltInEnv(cwd, env);

  // get stdio based on shell mode or silent log level
  const pipeMode = context.shell.logLevel === ShellLogLevel.silent
    || context.shell.mode === ShellMode.plain;
  const stdio = pipeMode ? Stdio.pipe : Stdio.inherit;

  // prepare options for each command
  const commandOptions: ProcessOptions[] = commands.map(
    cmd => {
      // expand env expressions e.g. $PATH = $PATH:./test
      const expandedEnv = expandDollarExpressions(cmd.env, env);
      // expand self env expressions
      const expandedSelfEnv = expandDollarExpressions(expandedEnv, expandedEnv);

      return {
        cwd,
        stdio,
        encoding: 'utf8',
        shell: true,
        env: { ...env, ...expandedSelfEnv }
      }
    }
  );

  // expand command vars with context vars
  commands.forEach(cmd => cmd.var = expandDollarExpressions(cmd.var, context.var));

  // apply cli args to last command
  if (taskArgs) commands[commands.length - 1].args = taskArgs;

  context.logger.executing(taskName);

  // assert mask entries exist in env or vars
  if (context.mask.length > 0) {
    const uniqueVars = commandOptions.reduce(
      (p, opts, i) => ({ ...opts.env, ...commands[i].var }),
      {}
    );

    assertMaskEntries(context.mask, uniqueVars)
  }

  // execute commands
  for (let index = 0; index < commands.length; index++) {
    const command = commands[index];
    const options = commandOptions[index];
    const step = context.output.nestedSteps ? command.step : `${index + 1}`;

    context.logger.step(step, command);

    await executeCommand(command, options, context);

    context.logger.line();
  }

  context.logger.completed(taskName, commands.length)
}

async function executeCommand(
  command: Command,
  options: ProcessOptions,
  context: TaskProcessContext
): Promise<void> {
  // ensure env.PATH is cross shell compatible
  options.env.PATH = createCrossShellPathEnv(options.env.PATH!);

  // expand var expressions into their raw values on to the command line
  let commandLine = expandVariableExpressions(command.cmd, command.var);

  // ensure command line is cross shell compatible
  commandLine = createCrossShellCommand(commandLine);

  const varsToMask = { ...options.env, ...command.var };
  try {
    await spawnProcess(
      `${commandLine} ${command.args}`.trim(),
      options,
      (data: Buffer) => context.logger.data(data.toString(), varsToMask)
    );
  } catch (error: any) {
    context.logger.error(error, varsToMask);
    context.logger.fail(command.name);
    process.exit(1);
  }
}