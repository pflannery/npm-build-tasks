import {
  CommandErrors,
  type OnSpawnData,
  type OnSpawnFail,
  type OnSpawnSuccess,
  type ProcessOptions
} from '#commands';
import { spawn } from 'node:child_process';

const { ChildProcessError, ChildProcessExited } = CommandErrors;

export function spawnProcess(
  command: string,
  options: ProcessOptions,
  onData: OnSpawnData
): Promise<void> {
  return new Promise((success: OnSpawnSuccess, fail: OnSpawnFail) => {
    const cp = spawn(command, [], <any>{ ...options });
    if (options.stdio === 'pipe') {
      cp.stdout.on('data', onData);
      cp.stderr.on('data', onData);
    }

    cp.once('error', e => fail(new ChildProcessError(e)));
    cp.once('close', code => {
      if (code === 0) return success();
      fail(new ChildProcessExited(code));
    });
  });
}