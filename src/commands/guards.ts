import { CommandErrors } from '#commands';
import type { KeyCollection } from '#utils';

export function assertMaskEntries(mask: string[], vars: KeyCollection<string>) {
  const varNames = Object.keys(vars);
  for (const maskVarName of mask) {
    if (varNames.includes(maskVarName) === false) {
      throw new CommandErrors.MaskVariableNotFound(maskVarName);
    }
  }
}