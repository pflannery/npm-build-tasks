export class MaskVariableNotFound extends Error {
  constructor(readonly maskVarName: string) {
    super(`Could not find the mask variable '${maskVarName}' in env or var`);
  }
}

export class ChildProcessExited extends Error {
  constructor(readonly exitCode: number | null) {
    super(`Exited with error code ${exitCode}`);
  }
}

export class ChildProcessError extends Error {
  constructor(readonly processError: Error) {
    super("An error occurred in the child process");
  }
}