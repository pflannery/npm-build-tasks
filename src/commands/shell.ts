import {
  replaceDollarEnvExprCmd,
  replaceDollarEnvExprPwsh,
  type KeyCollection
} from '#utils';
import { platform } from 'node:os';
import { delimiter } from 'node:path';

const platformShellInfo = getPlatformShellInfo();

export function getPlatformShellInfo() {
  const isWindows = platform() === 'win32';
  const ComSpec = process.env.ComSpec ?? 'C:\\Windows\\System32\\cmd.exe';
  const isCommandExe = isWindows && ComSpec.endsWith('cmd.exe');
  const isPwshExe = isWindows && (
    ComSpec.endsWith('powershell.exe')
    || ComSpec.endsWith('pwsh.exe')
  );

  return {
    isCommandExe,
    isPwshExe,
    isWindows
  };
}

export function applyBuiltInEnv(cwd: string, env: KeyCollection<string>) {
  // ensure the local bin commands are in the path
  env.PATH = `${process.env.PATH}${delimiter}./node_modules/.bin`;

  // set $PWD if missing
  env.PWD ??= cwd;
}

export function createCrossShellCommand(commandLine: string): string {
  // support windows shell env expressions
  if (platformShellInfo.isCommandExe)
    // replace $ expressions with '%%' for cmd.exe
    return replaceDollarEnvExprCmd(commandLine);
  else if (platformShellInfo.isPwshExe)
    // replace $ expressions with '${env:...}' for powershell
    return replaceDollarEnvExprPwsh(commandLine);
  else
    return commandLine;
}

const winPathDelimiterRegExp = /:(?!\\)+/g;
export function createCrossShellPathEnv(path: string) {
  return platformShellInfo.isWindows
    ? path.replaceAll(winPathDelimiterRegExp, delimiter).replaceAll(/\//g, '\\')
    : path.replaceAll(';', delimiter)
}