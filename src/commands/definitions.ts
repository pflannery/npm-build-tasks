import type { KeyCollection } from '#utils'

export type Command = {
  name: string
  cmd: string
  args: string
  step: string
  repeatIndex?: number
  var: KeyCollection<string>
  env: KeyCollection<string>
}

export type OnSpawnData = (data: Buffer) => void

export type OnSpawnSuccess = () => void

export type OnSpawnFail = (e: Error) => void

export enum Stdio {
  inherit = 'inherit',
  pipe = 'pipe'
}

export type ProcessOptions = {
  cwd: string
  env: KeyCollection<string>
  stdio: Stdio
  shell: boolean
}