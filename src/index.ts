#!/usr/bin/env node
import { CliErrors, convertArgvToRawInput, parseArgs } from '#cli';
import { CommandErrors, executeCommands } from '#commands';
import { LoggingErrors } from '#logging';
import {
  assertTaskNamesExists,
  createTaskProcessContext,
  TaskErrors
} from '#tasks';
import { UtilErrors, type KeyMap } from '#utils';

const cwd = process.cwd();

let context;
try {
  // parse cli arguments
  const opts = await parseArgs(convertArgvToRawInput(process.argv), cwd);

  // create the task process context
  context = await createTaskProcessContext(cwd, opts, true);
  assertTaskNamesExists(opts.taskNames, context.tasks);

  // execute task commands
  for (let index = 0; index < opts.taskNames.length; index++) {
    const taskName = opts.taskNames[index];
    const taskArgs = index === opts.taskNames.length - 1
      ? opts.taskArgs
      : undefined;

    await executeCommands(taskName, taskArgs, cwd, context);

    if (index < opts.taskNames.length - 1) {
      context.logger.line();
    }
  }

} catch (error: any) {
  // check if it's a handled error
  const handledErrors: KeyMap<any> = {
    ...TaskErrors,
    ...CommandErrors,
    ...CliErrors,
    ...UtilErrors,
    ...LoggingErrors
  };

  const isHandledError = Object.keys(handledErrors)
    .some((key: string) => error instanceof handledErrors[key])

  if (isHandledError)
    console.error(error.message)
  else
    console.error(new CliErrors.UnhandledError(error));

  // exit with fail code
  // must be '1' because powershell doesn't bubble up exit codes
  // and e2e tests won't work
  process.exit(1);

} finally {
  // close any open log file
  await context?.logger.dispose();
}