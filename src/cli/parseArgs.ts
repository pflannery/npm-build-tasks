import { createRulesetRegExp, switchRuleSet } from '#cli';
import { getInitialOptions } from '#options';

export async function parseArgs(args: string, cwd: string) {
  const options: any = getInitialOptions();

  // match and exec switch handlers
  const regex = createRulesetRegExp(switchRuleSet);
  while (true) {
    const match = regex.exec(args);
    if (!match) break;

    for (const key in match.groups) {
      const matched = match.groups[key];
      if (!matched) continue;
      await switchRuleSet[key].handler(match, options, cwd);
      break;
    }
  }

  const hasTaskNames = options.taskNames.length > 0;
  options.taskNames = hasTaskNames
    ? options.taskNames
    : [process.env.npm_lifecycle_event ?? ''];

  return options;
}

export function convertArgvToRawInput(argv: string[]): string {
  return argv.slice(2)
    .map(x => {
      const equalsPos = x.indexOf('=');
      const shellStrippedQuotes = equalsPos === x.length - 1
        || (equalsPos !== -1 && x.indexOf(' ') !== -1);

      if (shellStrippedQuotes === false) return x;

      const parts = x.split('=');
      return `${parts[0]}="${parts[1]}"`
    })
    .join(' ');
}