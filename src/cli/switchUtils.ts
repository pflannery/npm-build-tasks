import type { SwitchHandler, SwitchRule, SwitchRuleset } from '#cli';

/**
 * Creates a switch that has parameters
 * 
 * @param handler the handler used when the switch matches
 * @param match a regexp string that matches the parameter input
 * @param long long switch e.g. --env-file
 * @param short short switch e.g. -ef
 * @param help help text that is printed by the --help switch
 */
export const createSwitch = (
  handler: SwitchHandler,
  match: string,
  long?: string,
  short?: string,
  help?: string
): SwitchRule => {
  const switches = long && short
    ? `${long}|${short}`
    : long ?? '';

  const regex = match && switches
    ? `(?:${switches}) ${match}`
    : match ? match : switches

  return {
    handler,
    re: regex,
    long,
    short,
    help: help && switches
      ? `${switches}${help}`
      : help ?? ''
  }
};

/**
 * Creates a switch rule that has no parameters
 * 
 * @param handler the handler used when the switch matches
 * @param long long switch e.g. --env-file
 * @param short short switch e.g. -ef
 * @param help help text that is printed by the --help switch
 */
export const createNoParamSwitch = (
  handler: SwitchHandler,
  long: string,
  short?: string,
  help?: string
) => createSwitch(handler, '', long, short, help)

/**
 * Maps a switch ruleset to a named group regexp
 */
export function createRulesetRegExp(ruleset: SwitchRuleset): RegExp {
  const re = Object.keys(ruleset)
    .map((key: string) => {
      let re = ruleset[key].re;
      return `(?<${key}>${re})`;
    })
    .join('|');

  return new RegExp(re, 'g');
}