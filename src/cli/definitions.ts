import type { Options } from '#options';

export type SwitchHandler = (match: RegExpExecArray, options: Options, cwd: string) => Promise<void>

export type SwitchRule = {
  re: string
  handler: SwitchHandler
  long?: string
  short?: string
  help?: string
};

export type SwitchRuleset = { [key: string]: SwitchRule }

export enum SwitchGroup {
  taskFile = 'taskFile',
  envFile = 'envFile',
  env = 'env',
  var = 'var',
  shellMode = 'shellMode',
  logLevel = 'logLevel',
  logFile = 'logFile',
  logFileLevel = 'logFileLevel',
  styleDisable = 'styleDisable',
  styleStep = 'styleStep',
  styleError = 'styleError',
  outputNestedSteps = 'outputNestedSteps',
  outputDisableTaskTime = 'outputDisableTaskTime',
  version = 'version',
  help = 'help',
  listTasks = 'listTasks',
  taskArgs = 'taskArgs',
  invalid = 'invalid',
  taskName = 'taskName'
}