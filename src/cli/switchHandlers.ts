import {
  SwitchGroup,
  assertShellLogLevel,
  assertShellMode,
  assertSwitchValid,
  cliVersion,
  switchRuleSet
} from '#cli';
import { getShellLogLevel, getShellMode, type Options } from '#options';
import { loadTasksFromFile } from '#tasks';
import {
  assertVarName,
  trimEndsWith,
  trimStartsWith,
  type VariableCollection
} from '#utils';
import { EOL } from 'os';

function parseParams(match: string, collection: VariableCollection): void {
  const parts = match.split('=')
    .map(x => trimStartsWith(x, ' ', '"', "'"))
    .map(x => trimEndsWith(x, ' ', '"', "'"));

  const [name, value] = parts;
  assertVarName(name);

  collection[name] = value;
}

export async function tasksFileHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.taskFile;
  const taskFileSwitch = match.groups![SwitchGroup.taskFile];
  const path = trimStartsWith(taskFileSwitch, rule.short!, rule.long!).trim();
  opts.tasksFilePath = path;
}

export async function envFileHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.envFile;
  const envFileSwitch = match.groups![SwitchGroup.envFile];
  const path = trimStartsWith(envFileSwitch, rule.short!, rule.long!).trim();
  opts.envFilePath = path;
}

export async function envHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.env;
  const envSwitch = match.groups![SwitchGroup.env];
  const paramValue = trimStartsWith(envSwitch, rule.short!, rule.long!).trim();
  parseParams(paramValue, opts.env);
}

export async function varHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.var;
  const varSwitch = match.groups![SwitchGroup.var];
  const paramValue = trimStartsWith(varSwitch, rule.short!, rule.long!).trim();
  parseParams(paramValue, opts.var);
}

export async function shellModeHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.shellMode;
  const smSwitch = match.groups![SwitchGroup.shellMode];
  const mode = trimStartsWith(smSwitch, rule.short!, rule.long!).trim();
  assertShellMode(mode);
  opts.shell.mode = getShellMode(mode)
}

export async function logLevelHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.logLevel;
  const logLevelSwitch = match.groups![SwitchGroup.logLevel];
  const level = trimStartsWith(logLevelSwitch, rule.short!, rule.long!).trim();
  assertShellLogLevel(level);
  opts.shell.logLevel = getShellLogLevel(level)
}

export async function logFileHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.logFile;
  const logFileSwitch = match.groups![SwitchGroup.logFile];
  const path = trimStartsWith(logFileSwitch, rule.short!, rule.long!).trim();
  opts.log.filePath = path;
}

export async function logFileLevelHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.logFileLevel;
  const logFileLevelSwitch = match.groups![SwitchGroup.logFileLevel];
  const level = trimStartsWith(logFileLevelSwitch, rule.short!, rule.long!).trim();
  assertShellLogLevel(level);
  opts.log.logLevel = getShellLogLevel(level)
}

export async function styleDisableHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  opts.style.disable = true;
}

export async function styleStepHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.styleStep;
  const osSwitch = match.groups![SwitchGroup.styleStep];
  const style = trimStartsWith(osSwitch, rule.short!, rule.long!).trim();

  if (opts.style.step === undefined) opts.style.step = [];

  opts.style.step.push(style);
}

export async function styleErrorHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const rule = switchRuleSet.styleError;
  const osSwitch = match.groups![SwitchGroup.styleError];
  const style = trimStartsWith(osSwitch, rule.short!, rule.long!).trim();

  if (opts.style.error === undefined) opts.style.error = [];

  opts.style.error.push(style);
}

export async function outputNestedStepHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  opts.output.nestedSteps = true;
}

export async function outputDisableTaskTimeHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  opts.output.disableTaskTime = true;
}

export async function listTasksHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const tasksFileData = await loadTasksFromFile(cwd, opts.tasksFilePath, opts, {});
  const taskNames = Object.keys(tasksFileData.tasks);
  const message = `Tasks available:`
    + EOL
    + `  ${taskNames.sort().join(EOL + '  ')}`;

  console.log(message);
  process.exit(0);
};

export async function versionHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  console.log(cliVersion);
  process.exit(0);
}

export async function invalidHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  assertSwitchValid(match[0]);
}

export async function taskNameHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  opts.taskNames.push(match[0]);
}

export async function taskArgsHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  const taskArgsSwitch = match.groups![SwitchGroup.taskArgs];
  opts.taskArgs = trimStartsWith(taskArgsSwitch, ' -- ');
}

export async function helpHandler(match: RegExpExecArray, opts: Options, cwd: string) {
  // print usage
  console.log('Usage: task [options] <tasknames> [-- args]');
  console.log();

  // print options
  console.log('Options:');
  const rules = Object.entries(switchRuleSet);
  for (const [, rule] of rules) {
    const message = rule.help;
    if (!message) continue;
    console.log(message);
    console.log();
  }
  process.exit(0);
}