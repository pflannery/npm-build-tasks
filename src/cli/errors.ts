import { cliVersion } from '#cli';
import { getPossibleShellLogLevels, getPossibleShellModes } from '#options';
import { UtilErrors } from '#utils';

export class InvalidCliSwitch extends Error {
  constructor(readonly switchArg: string) {
    super(`Invalid switch '${switchArg}'`);
  }
}

export class InvalidShellLogLevel extends Error {
  constructor(readonly logLevel: string) {
    super(`Invalid log level '${logLevel}'. Valid levels are ${getPossibleShellLogLevels()}`);
  }
}

export class InvalidShellMode extends Error {
  constructor(readonly shellMode: string) {
    super(`Invalid shell mode '${shellMode}'. Valid modes are ${getPossibleShellModes()}`);
  }
}

export class UnhandledError extends UtilErrors.ErrorWithoutMessage {
  constructor(unhandled: Error) {
    super(
      unhandled,
      `An unexpected error occurred in js-build-tasks@${cliVersion}`
    );
  }
}