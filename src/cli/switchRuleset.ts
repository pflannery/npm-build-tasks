import {
  createNoParamSwitch,
  createSwitch,
  envFileHandler,
  envHandler,
  helpHandler,
  invalidHandler,
  listTasksHandler,
  logFileHandler,
  logFileLevelHandler,
  logLevelHandler,
  outputDisableTaskTimeHandler,
  outputNestedStepHandler,
  shellModeHandler,
  styleDisableHandler,
  styleErrorHandler,
  styleStepHandler,
  SwitchGroup,
  taskArgsHandler,
  taskNameHandler,
  tasksFileHandler,
  varHandler,
  versionHandler,
  type SwitchRuleset,
} from '#cli';
import { getPossibleShellLogLevels, getPossibleShellModes } from '#options';

const raw = String.raw;

export const switchRuleSet: SwitchRuleset = {
  [SwitchGroup.taskFile]: createSwitch(
    tasksFileHandler,
    raw`(.\S*)`,
    '--tasks-file', '-tf',
    " <path>\n  relative path to the tasks yaml file"
  ),
  [SwitchGroup.envFile]: createSwitch(
    envFileHandler,
    raw`(.\S*)`,
    '--env-file', '-ef',
    " <path>\n  relative path to the env file"
  ),
  [SwitchGroup.env]: createSwitch(
    envHandler,
    raw`(\w+=(?:['"].+?['"]|\S+))`,
    '--env', '-e',
    " <param=value>\n  overrides global env in the tasks file e.g. -e TEST=true -e OTHER=1"
  ),
  [SwitchGroup.var]: createSwitch(
    varHandler,
    raw`(\w+=(?:['"].+?['"]|\S+))`,
    '--var', '-v',
    " <param=value>\n  overrides global var in the tasks file e.g. -v TEST=true -v OTHER=1"
  ),
  [SwitchGroup.shellMode]: createSwitch(
    shellModeHandler,
    raw`(.\S*)`,
    '--shell-mode', '-sm',
    ` <mode>\n  ${getPossibleShellModes()}`
  ),
  [SwitchGroup.logLevel]: createSwitch(
    logLevelHandler,
    raw`(.\S*)`,
    '--log-level', '-l',
    ` <level>\n  log level can be: ${getPossibleShellLogLevels()}`
  ),
  [SwitchGroup.logFile]: createSwitch(
    logFileHandler,
    raw`(.\S*)`,
    '--log-file', '-lf',
    ` <path>\n  relative path to the log file`
  ),
  [SwitchGroup.logFileLevel]: createSwitch(
    logFileLevelHandler,
    raw`(.\S*)`,
    '--log-file-level', '-lfl',
    ` <level>\n  log level can be: ${getPossibleShellLogLevels()}`
  ),
  [SwitchGroup.styleDisable]: createNoParamSwitch(
    styleDisableHandler,
    '--style-disable', '-sd',
    `\n  disables styles`
  ),
  [SwitchGroup.styleStep]: createSwitch(
    styleStepHandler,
    raw`(.\S*)`,
    '--style-step', '-ss',
    ` <style>\n  style step info e.g. -ss yellowBright -ss bold`
  ),
  [SwitchGroup.styleError]: createSwitch(
    styleErrorHandler,
    raw`(.\S*)`,
    '--style-error', '-se',
    ` <style>\n  style errors e.g. -se red -se bold`
  ),
  [SwitchGroup.outputNestedSteps]: createNoParamSwitch(
    outputNestedStepHandler,
    '--output-nested-steps', '-ons',
    `\n  output nested step numbering e.g. > 1.1`
  ),
  [SwitchGroup.outputDisableTaskTime]: createNoParamSwitch(
    outputDisableTaskTimeHandler,
    '--output-disable-task-time', '-odtt',
    `\n  disable task start time in step output`
  ),
  [SwitchGroup.listTasks]: createNoParamSwitch(
    listTasksHandler,
    '--list-tasks', '-ls',
    `\n  list available tasks`
  ),
  [SwitchGroup.version]: createNoParamSwitch(
    versionHandler,
    '--version', undefined,
    `\n  shows the current version`
  ),
  [SwitchGroup.help]: createNoParamSwitch(
    helpHandler,
    '--help', '-h'
  ),
  [SwitchGroup.taskArgs]: createSwitch(
    taskArgsHandler,
    raw` -- .+$`
  ),
  [SwitchGroup.invalid]: createSwitch(
    invalidHandler,
    raw`(--|-)\w+`
  ),
  [SwitchGroup.taskName]: createSwitch(
    taskNameHandler,
    raw`\S+`
  ),
};