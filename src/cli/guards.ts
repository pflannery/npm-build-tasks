import { CliErrors } from '#cli';
import { isShellLogLevel, isShellMode } from '#options';

export function assertSwitchValid(arg: string) {
  throw new CliErrors.InvalidCliSwitch(arg);
}

export function assertShellLogLevel(testLogLevel: string) {
  const isNotDefined = testLogLevel === undefined || testLogLevel === null;
  if (isNotDefined || isShellLogLevel(testLogLevel.toString()) === false) {
    throw new CliErrors.InvalidShellLogLevel(testLogLevel);
  }
}

export function assertShellMode(testMode: string) {
  const isNotDefined = testMode === undefined || testMode === null;
  if (isNotDefined || isShellMode(testMode.toString()) === false) {
    throw new CliErrors.InvalidShellMode(testMode);
  }
}