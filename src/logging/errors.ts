import { ShellLogger } from '#logging';
import { UtilErrors } from '#utils';
import { EOL } from 'node:os';

export class LogErrorUnknown extends UtilErrors.ErrorWithoutMessage {
  constructor(readonly error: Error) {
    super(error, `${ShellLogger.name}: Unknown log error occurred`);
  }
}

export class LogFileError extends Error {
  constructor(logFilePath: string, readonly error: Error) {
    super(
      `Failed to write to log file '${logFilePath}'`
      + EOL
      + error.message
    );
  }
}