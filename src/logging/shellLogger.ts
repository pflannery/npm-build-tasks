import { CommandErrors, type Command } from '#commands';
import { LoggingErrors, type IWriter } from '#logging';
import { ShellLogLevel, type OutputOptions } from '#options';
import { maskValuesWithExpr, type KeyCollection } from '#utils';

export class ShellLogger {

  constructor(
    readonly writers: IWriter[],
    readonly mask: string[],
    readonly options: OutputOptions
  ) { }

  startedAt: number = 0;

  data(text: string, varsToMask: KeyCollection<string>) {
    let output = text;

    // mask requested var values in the output
    const maskInfo = getMaskedVars(varsToMask, this.mask);
    if (maskInfo.hasMaskedVars) output = maskValuesWithExpr(output, maskInfo.maskedVars);

    this.writers.forEach((x: IWriter) => x.write(ShellLogLevel.command, output));
  }

  line(text: string = '') {
    this.writers.forEach((x: IWriter) => x.writeLine(ShellLogLevel.step, text));
  }

  executing(taskName: string) {
    const output = `Executing [${taskName}]`;
    this.writers.forEach((x: IWriter) => x.writeLine(ShellLogLevel.step, output));
    this.line();
    this.startedAt = performance.now();
  }

  step(step: string, command: Command) {
    const startTime = Math.floor(performance.now() - this.startedAt) / 1000;
    const repeatInfo = command.repeatIndex === undefined ? '' : `[${command.repeatIndex}]`;
    const taskInfo = `[${command.name}]${repeatInfo} ${command.cmd}`;
    const output = this.options.disableTaskTime
      ? `${step}: ${taskInfo}`
      : `${step}: ${taskInfo} (${startTime}s)`;

    this.writers.forEach((x: IWriter) => x.writeLine(ShellLogLevel.step, output));
  }

  completed(taskName: string, taskRan: number) {
    const totalTime = Math.floor(performance.now() - this.startedAt) / 1000;
    const output = this.options.disableTaskTime
      ? `[${taskName}] completed ${taskRan} task(s)`
      : `[${taskName}] completed ${taskRan} task(s) (${totalTime}s)`

    this.writers.forEach((x: IWriter) => x.writeLine(ShellLogLevel.step, output));
  }

  fail(taskName: string) {
    const output = `>>>: [${taskName}] failed`;
    this.writers.forEach((x: IWriter) => x.writeError(ShellLogLevel.error, output));
  }

  error(
    error: CommandErrors.ChildProcessError | CommandErrors.ChildProcessExited | Error,
    varsToMask: KeyCollection<string>
  ) {
    let output = '';
    if (error instanceof CommandErrors.ChildProcessError) {
      output = error.processError.message;

      // mask requested var values in the output
      const maskInfo = getMaskedVars(varsToMask, this.mask);
      if (maskInfo.hasMaskedVars) output = maskValuesWithExpr(output, maskInfo.maskedVars);

    } else if (error instanceof CommandErrors.ChildProcessExited)
      // we don't need to mask exit code errors
      output = error.message;
    else
      throw new LoggingErrors.LogErrorUnknown(error);

    this.writers.forEach((x: IWriter) => x.writeError(ShellLogLevel.error, output));
  }

  async dispose() {
    for (const writer of this.writers) {
      await writer.dispose()
    }
  }

}

function getMaskedVars(vars: KeyCollection<string>, mask: string[]) {
  let hasMaskedVars = false;
  const maskedVars: KeyCollection<string> = {};

  for (const key in vars) {
    if (mask.includes(key) === false) continue;
    maskedVars[key] = vars[key];
    hasMaskedVars = true;
  }

  return { hasMaskedVars, maskedVars };
}