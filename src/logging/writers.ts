import { LoggingErrors } from '#logging';
import type { Options } from '#options';
import { terminalWrite, terminalWriteLine } from '#utils';
import { WriteStream, createWriteStream } from 'node:fs';
import { EOL } from 'node:os';
import { stripVTControlCharacters } from 'node:util';

export interface IWriter {
  logLevel: number
  write: (level: number, text: string) => void
  writeLine: (level: number, text?: string) => void
  writeError: (level: number, error: string) => void
  dispose: () => Promise<void>
}

// ----------------------------------------------------------------------------
export class FileWriter implements IWriter {

  constructor(readonly options: Options) {
    this.logLevel = options.log.logLevel;
    this.stream = createWriteStream(options.log.filePath, { flags: 'w' });

    // handle error event
    this.stream.on('error', this.onError.bind(this));
  }

  readonly logLevel: number;

  private stream: WriteStream;

  write(level: number, text: string) {
    if (this.logLevel > level) return;

    // remove raw styles from child command processes
    const ansi = stripVTControlCharacters(text);
    this.stream.write(ansi);
  }

  writeLine(level: number, text?: string) {
    if (this.logLevel > level) return;
    this.stream.write(text + EOL);
  }

  writeError(level: number, error: string) {
    if (this.logLevel > level) return;
    this.stream.write(error + EOL);
  }

  onError(e: any) {
    console.error(new LoggingErrors.LogFileError(this.options.log.filePath, e));
    process.exit(1);
  }

  dispose(): Promise<void> {
    return new Promise(success => this.stream.end(success));
  }

}

// ----------------------------------------------------------------------------
export class ProcessWriter implements IWriter {

  constructor(readonly options: Options) {
    this.logLevel = options.shell.logLevel;
  }

  readonly logLevel: number;

  write(level: number, text: string) {
    if (this.logLevel > level) return;
    terminalWrite(text);
  }

  writeLine(level: number, text?: string) {
    if (this.logLevel > level) return;
    if (!text)
      terminalWriteLine(text);
    else
      terminalWriteLine(text, this.options.style.step);
  }

  writeError(level: number, error: string) {
    if (this.logLevel > level) return;
    terminalWriteLine(error, this.options.style.error);
  }

  dispose(): Promise<void> { return Promise.resolve() }

}