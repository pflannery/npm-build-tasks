// levels
const step = 0;
const command = 1;
const error = 2;
const silent = 3;

export const ShellLogLevel = {
  step,
  command,
  error,
  silent
};

export const ShellLogLevelNames = {
  step: 'step',
  command: 'command',
  error: 'error',
  silent: 'silent'
};

// utils
export const isShellLogLevel = (level: string): boolean => {
  return Reflect.has(ShellLogLevel, level.toLowerCase());
}

export const getShellLogLevel = (level: string): number => {
  return Reflect.get(ShellLogLevel, level.toLowerCase());
}

export const getPossibleShellLogLevels = (): string => {
  return `${Object.keys(ShellLogLevel).join('|')}`;
}