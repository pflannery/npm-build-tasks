import { trimEndsWith } from '#utils';
import { EOL } from 'os';
import { inspect } from 'util';

export const Style = inspect.colors;

export function getPrintedStyles(): string {
  let index = 1;
  const output = [];
  const colorsSorted = Object.keys(Style).sort()

  for (const color of colorsSorted) {
    const modulo = index % 3;
    if (modulo % 2 === 0) output.push(' ');
    output.push(color, ',');
    if (modulo === 0) output.push(EOL);

    index++;
  }

  return trimEndsWith(output.join(''), ',');
}