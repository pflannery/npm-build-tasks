import type { ShellMode } from '#options';
import type { VariableCollection } from '#utils';

export type StyleOptions = {
  disable?: boolean
  step: string[]
  error: string[]
}

export type OutputOptions = {
  nestedSteps: boolean
  disableTaskTime?: boolean
}

export type ShellOptions = {
  mode: ShellMode
  logLevel: number
}

export type LogFileOptions = {
  filePath: string
  logLevel: number
}

export type Options = {
  tasksFilePath: string
  envFilePath?: string
  taskNames: string[]
  taskArgs: string
  shell: ShellOptions
  log: LogFileOptions
  output: OutputOptions
  style: StyleOptions
  env: VariableCollection
  var: VariableCollection
}