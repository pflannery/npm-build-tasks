export enum ShellMode {
  tty = "tty",
  plain = "plain"
}

// utils
export const isShellMode = (mode: string): boolean => {
  return Reflect.has(ShellMode, mode.toLowerCase());
}

export const getShellMode = (mode: string): ShellMode => {
  return Reflect.get(ShellMode, mode.toLowerCase());
}

export const getPossibleShellModes = (): string => {
  return `${Object.keys(ShellMode).join('|')}`;
}