import {
  ShellLogLevel,
  ShellMode,
  type LogFileOptions,
  type Options,
  type OutputOptions,
  type ShellOptions,
  type StyleOptions
} from '#options';

export const getInitialOptions = (): any => ({
  tasksFilePath: getDefaultOptions().tasksFilePath,
  taskNames: [],
  taskArgs: '',
  shell: {} as any,
  log: {} as any,
  output: {} as any,
  style: {} as any,
  env: {},
  var: {},
})

export const getDefaultStyleOptions = (): StyleOptions => ({
  step: [],
  error: ['red']
})

export const getDefaultShellOptions = (): ShellOptions => ({
  mode: ShellMode.tty,
  logLevel: ShellLogLevel.step
})

export const getDefaultOutputOptions = (): OutputOptions => ({
  nestedSteps: false,
  disableTaskTime: false
})

export const getDefaultLogOptions = (): LogFileOptions => ({
  filePath: '',
  logLevel: ShellLogLevel.step
})

export const getDefaultOptions = (): Options => ({
  tasksFilePath: './tasks.yml',
  taskNames: [],
  taskArgs: '',
  shell: getDefaultShellOptions(),
  log: getDefaultLogOptions(),
  output: getDefaultOutputOptions(),
  style: getDefaultStyleOptions(),
  env: {},
  var: {},
})