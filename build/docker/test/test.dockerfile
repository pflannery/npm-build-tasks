# current node. see https://nodejs.org/en/about/previous-releases
FROM node:current-alpine
ARG TARGET_PATH=/js-build-tasks

# update os packages
RUN apk update && apk upgrade

# copy in project files (minus the .dockerignore entries)
COPY / $TARGET_PATH

# set the $CWD to the project root
WORKDIR $TARGET_PATH

# update npm to latest
RUN npm install -g npm@10 js-build-tasks

# install dependencies
RUN npm ci

# run the tests
CMD task test