# Debian code names https://wiki.debian.org/DebianReleases#Production_Releases
# Node code names https://nodejs.org/en/about/previous-releases
ARG NODE_DEBIAN_TAG=current-bookworm
FROM node:${NODE_DEBIAN_TAG}
ARG TARGET_PATH=/js-build-tasks

# update os packages
RUN apt-get -y update && apt-get -y upgrade

# copy in project files (minus the .dockerignore entries)
COPY / $TARGET_PATH

# set the $CWD to the project root
WORKDIR $TARGET_PATH

# update npm to latest
RUN npm install -g npm@10 js-build-tasks

# install dependencies
RUN npm ci

RUN task compile

# run the tests against self
CMD node ./out/src/index.js test