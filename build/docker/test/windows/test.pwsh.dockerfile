# see ./tasks.yml for example of how to run on local docker (windows container)
FROM mcr.microsoft.com/powershell:lts-7.2-nanoserver-ltsc2022
ARG NODE_ZIP=https://nodejs.org/dist/v20.14.0/node-v20.14.0-win-x64.zip
ARG NODE_PATH="C:\Program Files\nodejs"

# add node to PATH
ENV PATH "C:\Windows\system32;C:\Program Files\PowerShell;${NODE_PATH}"

# download nodejs
RUN curl.exe -o node.zip %NODE_ZIP% 
RUN mkdir "%NODE_PATH%"
RUN tar.exe -xf node.zip -C "%NODE_PATH%" --strip-components=1

ARG TARGET_PATH=/js-build-tasks

# copy in project files (minus the .dockerignore entries)
COPY / $TARGET_PATH

# set the $CWD to the project root
WORKDIR $TARGET_PATH

# update npm to latest
RUN npm install -g npm@10 js-build-tasks

# install dependencies
RUN npm ci

# make powershell echo work like every other terminal
RUN mkdir C:\\Users\\ContainerUser\\Documents\\WindowsPowerShell\\Microsoft
RUN echo Set-Alias -Name echo -Value Write-Host -Option AllScope; > "C:\\Program Files\\PowerShell\\Profile.ps1"

# set the ComSpec to tell node what shell to use
ENV ComSpec "C:\Program Files\PowerShell\pwsh.exe"

# run the tests
CMD ["pwsh.exe", "-NoLogo", "-ExecutionPolicy", "Bypass", "-Command", "task test"]