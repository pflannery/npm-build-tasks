# see ./build/publish.tasks.yml for example of how to run on local docker
FROM node:22-alpine3.19
ARG TARGET_PATH=/js-build-tasks

# update os packages
RUN apk update && apk upgrade

# copy in project files (minus the .dockerignore entries)
COPY / $TARGET_PATH

# set the $CWD to the project root
WORKDIR $TARGET_PATH

# update npm to latest
RUN npm install -g npm js-build-tasks

# install dependencies
RUN npm ci

# test using bundle
RUN task test:publish

# publish
CMD npm publish