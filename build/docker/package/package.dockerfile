# see "docker:package" task in ./tasks.yml on how this docker container is ran
FROM node:22-alpine3.19
ARG TARGET_PATH=/js-build-tasks
ENV PACKAGE_OUT_PATH=.package

# update os packages
RUN apk update && apk upgrade

# copy in project files (minus the .dockerignore entries)
COPY / $TARGET_PATH

# set the $CWD to the project root
WORKDIR $TARGET_PATH

# update npm to latest
RUN npm install -g npm js-build-tasks

# install dependencies
RUN npm ci

# test using bundle
RUN task test:publish

# simulate prepublish
RUN task prepublishOnly

# create .package folder
RUN mkdir $PACKAGE_OUT_PATH

# package and move to .package folder
CMD npm pack && mv *.tgz $PACKAGE_OUT_PATH