#!/usr/bin/env node
import { readFileSync, writeFileSync } from 'fs';

// read the package.json file
const pkg = JSON.parse(readFileSync('./package.json', 'utf-8'));

// remove import subpaths for bundled code (no paths are required)
delete pkg.imports;

// overwrite package.json
writeFileSync('./package.json', JSON.stringify(pkg, null, '  '));