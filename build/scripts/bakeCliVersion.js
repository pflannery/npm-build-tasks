#!/usr/bin/env node
import { readFileSync, writeFileSync } from 'fs';

// read the package.json file
const pkg = JSON.parse(readFileSync('./package.json', 'utf-8'));

const jsContent = `export const cliVersion = '${pkg.version}';`;

// overwrite package.json
writeFileSync('./src/cli/version.ts', jsContent);