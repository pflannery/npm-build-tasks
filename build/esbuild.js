import esbuild from 'esbuild';
import { resolve } from 'node:path';

const projectPath = process.cwd();
const sourcePath = resolve(projectPath, 'src');
const distPath = resolve(projectPath, 'dist');
const isDevEnv = process.env?.BUNDLE_DEV;
const sourceEntryPoint = resolve(sourcePath, 'index.ts');
const outputFile = 'cli.bundle.js';
const minify = !isDevEnv;

await esbuild.build({
  entryPoints: [sourceEntryPoint],
  outfile: resolve(distPath, outputFile),
  platform: 'node',
  format: 'esm',
  external: ['js-yaml'],
  mainFields: ['module', 'main'],
  sourcemap: 'linked',
  bundle: true,
  minify
});