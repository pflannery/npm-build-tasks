# Js build tasks

[![Pipeline status](https://gitlab.com/js-build/js-build-tasks/badges/master/pipeline.svg)](https://gitlab.com/js-build/js-build-tasks/-/pipelines)
[![The ISC license](https://img.shields.io/badge/license-ISC-orange.png?color=blue&style=flat-square)](http://opensource.org/licenses/ISC)
[![NPM version](https://img.shields.io/npm/v/js-build-tasks.svg)](https://www.npmjs.org/package/js-build-tasks)
[![NPM downloads](https://img.shields.io/npm/dm/js-build-tasks.svg)](https://npmjs.org/package/js-build-tasks "View this project on NPM")

[![BuyMeACoffee](https://www.buymeacoffee.com/assets/img/custom_images/purple_img.png)](https://www.buymeacoffee.com/peterf)

A library for running build tasks and scripts for nodejs

### Contents

- [Example](#example)
- [Install](#install)
- [Tasks file parameters](#tasks-file-parameters)
- [Command line options](#command-line-options)
- [Styles available](#styles-available)

## Example

```yaml
# tasks.yml example
extends: ./build/docker.tasks.yml # optional

log: # optional
  filePath: ./output.log
  logLevel: step

tasks: # required
  clean: rimraf ./out ./dist .artifacts
  compile: [clean, tsc]
```

## Install

##### As a global tool

To run tasks directly in the terminal install globally

> `npm install -g js-build-tasks`
> 
> run directly in the terminal
> 
> `task clean compile`

##### As a dev package

To run tasks using package.json scripts install as dev dependency

> `npm install -save-dev js-build-tasks`

##### Run via package.json scripts

```jsonc
// package.json
{
  "scripts": {
    "clean": "task",
    "compile": "task",
    "test": "task"
  }
}
```

## Tasks file parameters

```yaml
# optional: extends tasks and options from other tasks files.
# type: string | string[]
extends: <relative-file-path>

# optional: shell options
shell:

  # optional: type of shell output
  # type: string
  # - tty (default)
  # - plain
  # [extends]: overrides extended files
  mode: <tty|plain>

  # optional: output log level
  # type: string
  # - step: prints step info, command output and errors (default)
  # - command: prints command output and errors
  # - error: prints errors only
  # - silent: no output is printed
  # [extends]: overrides extended files
  logLevel: <step|command|error|silent>

log:

  # optional: log file path
  # type: string
  # - auto creates it's dir(s) if they don't exist
  # - strips ansi escape codes from output
  # - enforces shell mode '-sm plain'
  # [extends]: overrides extended files
  filePath: <relative-file-path>

  # optional: file log level
  # type: string
  # - step: writes step info, command output and errors (default)
  # - command: writes command output and errors
  # - error: writes errors only
  # [extends]: overrides extended files
  logLevel: <step|command|error>

# optional: output options
output:

  # optional: when true, will print a nested number format e.g. 1.1.1
  # type: boolean
  # useful for tracing more complex task group steps
  # [extends]: overrides extended files
  nestedSteps: <true|false>

  # optional: disables the task start time in the 'step info' printed in the output
  # type: string
  # [extends]: overrides extended files
  disableTaskTime: <true|false>

# optional: style 'step' and 'error' logging printed to the output
# type: string[] (See valid styles below)
# NOTE doesn't apply to the command output
# [extends]: overrides extended files
style: 
  step: []
  error: [red]

# optional: env(s) available to all tasks
# type: [key: string]: string | boolean | number
# - .node_modules/.bin is automatically added to the $PATH to allow local bin execution
# - expands $ expressions e.g. $PATH: "$PATH:./my/path" will append './my/path' to $PATH
# - shell specific
#   - bash|sh: env is placed on to the command line as is. e.g. echo $env
#   - cmd.exe: '$MY_ENV' is transformed to '%MY_ENV%' on the command line
#   - powershell.exe|pwsh.exe: '$MY_ENV' is transformed to '${env:MY_ENV}' on the command line
# - windows: $PATH env sanitizing e.g. './bin/path1:./bin/path2' becomes '.\bin\path1;.\bin\path2'
# [extends]: mixed in with extended files or overrides if matching the same name
env: { MY_ENV: 1 } # usage echo $MY_ENV or echo ${MY_ENV}

# optional: var(s) available to all tasks (never put into env)
# type: [key: string]: string | boolean | number
# variables are expanded on to the command line e.g. 'echo $MY_VAR' becomes 'echo var value'
# [extends]: mixed in with extended files or overrides if matching the same name
var: { MY_VAR: var value } # usage echo $MY_VAR or echo ${MY_VAR}

# optional: array of 'env' and\or 'var' names to mask in the printed output.
# type: string[]
# e.g. this prevents any command line errors printing sensitive values into the output.
# NOTE: if you trust the command your using then you probably won't need this
# enforces shell mode '-sm plain'
# [extends]: mixed in with extended files
mask: [MY_ENV, MY_VAR, ...]

# required
# type: [key: string]: string | string[]
# [extends]: mixed in with extended files or overrides if matching the same name
tasks:

  # direct statements
  clean: rimraf ./out ./dist

  # group statements (yaml array)
  compile: [clean, tsc]
  test: [echo $MODE, compile, node ./out/test/runner.js]

  # scope `env` or `var`
  # NOTE: overrides global entries that have the same <param> name within the task group scope 
  test:dev:
    - env: { MODE: dev }
    - env.file: ./some/path/.env # optionally load env from a file into the scope
    - test # scoped env is available to all child tasks
    - echo exit scope # $MODE=dev won't be set in parent tasks

  # scope `repeat`
  # repeats commands after the repeat statement
  # NOTE: will repeat the `product` of multiple arrays
  test:build:
    - repeat:
        # type: [key: string]: Array<string | boolean | number>
        # NOTE: keys are treated as `var` per iteration
        CODE_NAME: [bookworm, bullseye, buster]
    - echo $CODE_NAME # will be executed x times based on the array product of the repeat

  # package scripts
  # The following example: package.json must contain { "scripts": { "prepublishOnly": "task" } }
  # NOTE: the packageManager field in the package.json is used to execute the task
  # default is 'npm'
  prepublishOnly: test
```

## Command line options

Cli switches override global settings in the tasks file

##### Tasks
`<tasknames> [-- args]`
> task(s) that will be run. e.g. `task clean compile`
>
> optional args can be appended to the end of the command line. e.g `task clean -- ./out`
>
> NOTE args will be applied to the last command that is executed

##### Version
`--version`
> prints the cli version e.g. `task --version`

##### Help
`--help`
> prints the cli switch usage and options e.g. `task --help`

##### List tasks
`--list-tasks|-ls`
> prints the tasks available `task -ls`
>
> can be combined with the -tf switch

##### Task file
`--tasks-file|-tf <path>`
> relative path to the tasks yaml file based on the cwd. default is ./tasks.yml

##### Shell mode
`--shell-mode|-sm <mode>`
> - `tty` (default)
> - `plain`

##### Shell log level
`--log-level|-l <level>`
> - `step` shows step info, command output and errors (default)
> - `command` shows command output and errors
> - `error` shows command errors only
> - `silent` silent output

##### Log file
`--log-file|-lf <path>`
> relative path to a log file based on the cwd
> - auto creates it's dir(s) if they don't exist
> - strips ansi escape codes from output
> - enforces shell mode '-sm plain'

##### Log file level
`--log-file-level|-lfl <level>`
> - `step` writes step info, command output and errors (default)
> - `command` writes command output and errors
> - `error` writes command errors only

##### Env file
`--env-file|-ef <path>`
> relative path to an env file based on the cwd

##### Env
`--env|-e <param=value>`
> env $ expressions available to all tasks e.g. `-e TEST=true -e OTHER=1`

##### Var
`--var|-v <param=value>`
> var $ expressions available to all tasks e.g. `-v TEST=true -v OTHER=1`

##### Output nested step numbering
`--output-nested-steps|-ons`
> output nested step numbering e.g. `> 1.1`

##### Style disable
`--style-disable|-sd`
> disable styles

##### Style step
`--style-step|-ss <style>`
> style step info e.g. `-ss yellowBright -ss bold`

##### Style error
`--style-error|-se <style>`
> style errors e.g. `-se yellowBright -se bold`

### Styles available:

Uses nodejs text [styles](https://nodejs.org/docs/latest-v20.x/api/util.html#util_modifiers)

>  'bgBlack',         'bgBlue',        'bgBlueBright',<br>
>  'bgCyan',          'bgCyanBright',  'bgGray',<br>
>  'bgGreen',         'bgGreenBright', 'bgMagenta',<br>
>  'bgMagentaBright', 'bgRed',         'bgRedBright',<br>
>  'bgWhite',         'bgWhiteBright', 'bgYellow',<br>
>  'bgYellowBright',  'black',         'blink',<br>
>  'blue',            'blueBright',    'bold',<br>
>  'cyan',            'cyanBright',    'dim',<br>
>  'doubleunderline', 'framed',        'gray',<br>
>  'green',           'greenBright',   'hidden',<br>
>  'inverse',         'italic',        'magenta',<br>
>  'magentaBright',   'overlined',     'red',<br>
>  'redBright',       'reset',         'strikethrough',<br>
>  'underline',       'white',         'whiteBright',<br>
>  'yellow',          'yellowBright'<br>